<?php
namespace myApp0\Usuario;

use \FDSoil\DbFunc as DbFunc;

class Rol extends \myApp0\Usuario\Rol\Menu
{
    
    private function path() { return "../../../".$_SESSION['aMyApp'][0]."/class/Usuario/sql/menu/rol/"; }

    public function rolList() { return DbFunc::exeQryFile(self::path()."list_select.sql", null); }

    public function rolOptNivel_0() { return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."opt_0_select.sql", $_POST))); }

    public function rolOptNivel_1() { return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."opt_1_select.sql", $_POST))); }

    public function rolOptNivel_2() { return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."opt_2_select.sql", $_POST))); }

    public function rolOptNivel_3() { return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."opt_3_select.sql", $_POST))); }

    public function rolRow() { return DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."row_select.sql", $_POST)); }

    public function rolRegister()
    {
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."row_register.sql", $_POST));
        return $row[0];        
    }
    
    public function rolDelete()
    {
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."row_delete.sql", $_POST));
        return $row[0];        
    }

    public function rolOpt() 
    {
	$strArray0 = self::rolOptNivel_0();
	$strArray1 = self::rolOptNivel_1();
	$strArray2 = self::rolOptNivel_2();
	$strArray3 = self::rolOptNivel_3();
	$strArray0 = substr($strArray0,0,strlen($strArray0));
	$strArray1 = substr($strArray1,0,strlen($strArray1));
	$strArray2 = substr($strArray2,0,strlen($strArray2));
	$strArray3 = substr($strArray3,0,strlen($strArray3));
        return '['.$strArray0.','.$strArray1.','.$strArray2.','.$strArray3.']';   
    }
    
}

