<?php
namespace myApp0\Usuario\Rol;

use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class Menu
{

    /** MENÚ
    * @author Ernesto Jiménez <fdsoil123@gmail.com>*/
    private function path() { return "../../../".$_SESSION['aMyApp'][0]."/class/Usuario/sql/menu/"; } 

    /** Hacer menú.
    * Descripción: Genera toda la información para crear el menú dinámicamente.
    * @param array $aId Arreglo que contiene el ID de rol (perfil) del usuario.
    * @return result Devuelve toda la información para crear el menú dinámico.*/   
    function menuMake($aId)
    {
        $strArray0=self::menuNivel_0_Crea($aId);
        $strArray1=self::menuNivel_1_Crea($aId);
        $strArray2=self::menuNivel_2_Crea($aId);
        $strArray3=self::menuNivel_3_Crea($aId);  
        $strArray='['.$strArray0.','.$strArray1.','.$strArray2.','.$strArray3.']'; 
	$strArray=str_replace("FDSoil",strtolower($_SESSION['FDSoil']),$strArray);
	$strArray=str_replace("appOrg",strtolower($_SESSION['appOrg']),$strArray);
        $arr = $_SESSION['aMyApp'];
        $len = count($arr);
        for ( $i = 0; $i < $len ; ++$i )
	    $strArray=str_replace("myApp".$i,strtolower($arr[$i]),$strArray);        
	return $strArray;				
    }

    /** Lista del menú del nivel 0 para llenar el combo.
    * Descripción: Lista del menú del nivel 0 (cero) para llenar el combo..
    * @return result Resultado del query que genera la lista del menú del nivel 0 para llenar el combo.*/
    function menuNivel_0_combo()
    {
        return DbFunc::exeQryFile(self::path()."n0_combo_select.sql", null);
    }

    /** Datos para crear el menú dinámico del nivel 0.
    * Descripción: Datos para crear el menú dinámico del nivel 0 (cero).
    * @param array $aId Arreglo que contiene el ID de rol (perfil) del usuario.
    * @return result Devuelve los datos para crear el menú dinámico del nivel 0.*/  
    function menuNivel_0_Crea($aId)
    {    
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile( self::path()."n0_select.sql", $aId)));
    }

    /** Título del menú del nivel 0.
    * Descripción: Título del menú del nivel 0.
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera el título del menú del nivel 0.*/
    function menuNivel_0_idTitulo($aId)
    {
        return DbFunc::exeQryFile(self::path()."n0_id_titulo_select.sql", $aId);
    }

    /** Lista del menú del nivel 0.
    * Descripción: Lista del menú del nivel 0 (cero).
    * @return result Resultado del query que genera la lista del menú del nivel 0.*/
    function menuNivel_0_list()
    {
        return DbFunc::exeQryFile(self::path()."n0_list_select.sql", null);
    }

    /** Lista de todas las opciones de menú del nivel 0.
    * Descripción: Lista de todas las opciones del menú del nivel 0 (cero) para seleccionar el perfil del usuario.
    *
    * Nota: No recibe parametros.
    * @return result Resultado del query que genera la lista de todas las opciones del menú del nivel 0.*/
    function menuNivel_0_optList()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."n0_opt_select.sql",null)));
    }

    /** Borra registro nivel 0.
    * Descripción: Borrarr el registro del nivel 0 (cero).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_0_rowDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n0_delete_pl.sql", $_POST));
	return $row[0];
    }

    /** Edita registro del menú del nivel 0.
    * Descripción: Edita registro del menú del nivel 0 (cero).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return result Resultado del query que genera registro del menú del nivel 0.*/
    function menuNivel_0_rowEdit()
    {
        return DbFunc::exeQryFile(self::path()."n0_row_select.sql", $_POST);
    }

    /** Salva registro nivel 0.
    * Descripción: Salvar el registro del nivel 0 (cero).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_0_rowSave()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n0_registro_pl.sql", $_POST));
	return $row[0];
    }

    /** Datos para crear el menú dinámico del nivel 1.
    * Descripción: Datos para crear el menú dinámico del nivel 1 (uno).
    * @param array $aId Arreglo que contiene el ID de rol (perfil) del usuario.
    * @return result Devuelve los datos para crear el menú dinámico del nivel 1.*/ 
    function menuNivel_1_Crea($aId)
    {
        return json_encode( DbFunc::fetchAllRow(DbFunc::exeQryFile( self::path()."n1_select.sql", $aId)));
    }

    /** Título del menú del nivel 1.
    * Descripción: Título del menú del nivel 1.
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera el título del menú del nivel 1.*/
    function menuNivel_1_idTitulo($aId)
    {
        return DbFunc::exeQryFile(self::path()."n1_id_titulo_select.sql", $aId);
    }

    /** Lista del menú del nivel 1 para llenar el combo.
    * Descripción: Lista del menú del nivel 1 (uno) para llenar el combo.
    * @param array $aId Arreglo que contiene el ID del menú padre.
    * @return result Resultado del query que genera la lista del menú del nivel 1 para llenar el combo.*/
    function menuNivel_1_list_combo($aId)
    {
        return DbFunc::exeQryFile(self::path()."n1_list_combo_select.sql", $aId);
    }

    /** Lista del menú del nivel 1.
    * Descripción: Lista del menú del nivel 1 (uno).
    * @param array $aId Arreglo que contiene el ID del menú padre.
    * @return result Resultado del query que genera la lista del menú del nivel 1.*/
    function menuNivel_1_list_tabla($aId)
    {
        return DbFunc::exeQryFile(self::path()."n1_list_tabla_select.sql", $aId);
    }

    /** Lista de todas las opciones de menú del nivel 1.
    * Descripción: Lista de todas las opciones del menú del nivel 1 (uno) para seleccionar el perfil del usuario.
    *
    * Nota: No recibe parametros.
    * @return result Resultado del query que genera la lista de todas las opciones del menú del nivel 1.*/
    function menuNivel_1_optList()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."n1_opt_select.sql",null)));
    }

    /** Borra registro nivel 1.
    * Descripción: Borrarr el registro del nivel 1 (uno).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_1_rowDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n1_delete_pl.sql", $_POST));
	return $row[0];
    }

    /** Edita registro del menú del nivel 1.
    * Descripción: Edita registro del menú del nivel 1 (uno).
    *
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera registro del menú del nivel 1.*/
    function menuNivel_1_rowEdit($aId)
    {
        return DbFunc::exeQryFile(self::path()."n1_row_select.sql", $aId);
    }

    /** Salva registro nivel 1.
    * Descripción: Salvar el registro del nivel 1 (uno).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_1_rowSave()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n1_registro_pl.sql", $_POST));
	return $row[0];
    }

    /** Datos para crear el menú dinámico del nivel 2.
    * Descripción: Datos para crear el menú dinámico del nivel 2 (dos).
    * @param array $aId Arreglo que contiene el ID de rol (perfil) del usuario.
    * @return result Devuelve los datos para crear el menú dinámico del nivel 2.*/    
    function menuNivel_2_Crea($aId)
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile( self::path()."n2_select.sql", $aId)));
    }

    /** Título del menú del nivel 2.
    * Descripción: Título del menú del nivel 2.
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera el título del menú del nivel 2.*/
    function menuNivel_2_idTitulo($aId)
    {
        return DbFunc::exeQryFile(self::path()."n2_id_titulo_select.sql", $aId);
    }

    /** Lista del menú del nivel 2 para llenar el combo.
    * Descripción: Lista del menú del nivel 2 (dos) para llenar el combo.
    * @param array $aId Arreglo que contiene el ID del menú padre.
    * @return result Resultado del query que genera la lista del menú del nivel 2 para llenar el combo.*/
    function menuNivel_2_list_combo($aId)
    {
        return DbFunc::exeQryFile(self::path()."n2_list_combo_select.sql", $aId);
    }

    /** Lista del menú del nivel 2.
    * Descripción: Lista del menú del nivel 2 (dos).
    * @param array $aId Arreglo que contiene el ID del menú padre.
    * @return result Resultado del query que genera la lista del menú del nivel 2.*/
    function menuNivel_2_list_tabla($aId)
    {
        return DbFunc::exeQryFile(self::path()."n2_list_tabla_select.sql", $aId);
    }

    /** Lista de todas las opciones de menú del nivel 2.
    * Descripción: Lista de todas las opciones del menú del nivel 2 (dos) para seleccionar el perfil del usuario.
    *
    * Nota: No recibe parametros.
    * @return result Resultado del query que genera la lista de todas las opciones del menú del nivel 2.*/
    function menuNivel_2_optList()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."n2_opt_select.sql",null)));
    }

    /** Borra registro nivel 2.
    * Descripción: Borrarr el registro del nivel 2 (dos).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_2_rowDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n2_delete_pl.sql", $_POST));
	return $row[0];
    }

    /** Edita registro del menú del nivel 2.
    * Descripción: Edita registro del menú del nivel 2 (dos).
    *
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera registro del menú del nivel 2.*/
    function menuNivel_2_rowEdit($aId)
    {
        return DbFunc::exeQryFile(self::path()."n2_row_select.sql", $aId);
    }

    /** Salva registro nivel 2.
    * Descripción: Salvar el registro del nivel 2 (dos).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_2_rowSave()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n2_registro_pl.sql", $_POST));
	return $row[0];
    }

    /** Datos para crear el menú dinámico del nivel 3.
    * Descripción: Datos para crear el menú dinámico del nivel 3 (dos).
    * @param array $aId Arreglo que contiene el ID de rol (perfil) del usuario.
    * @return result Devuelve los datos para crear el menú dinámico del nivel 3.*/   
    function menuNivel_3_Crea($aId)
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile( self::path()."n3_select.sql", $aId)));
    }

    /** Lista del menú del nivel 3.
    * Descripción: Lista del menú del nivel 3 (tres).
    * @param array $aId Arreglo que contiene el ID del menú padre.
    * @return result Resultado del query que genera la lista del menú del nivel 3.*/
    function menuNivel_3_list_tabla($aId)
    {
        return DbFunc::exeQryFile(self::path()."n3_list_tabla_select.sql", $aId);
    }

    /** Lista de todas las opciones de menú del nivel 3.
    * Descripción: Lista de todas las opciones del menú del nivel 3 (tres) para seleccionar el perfil del usuario.
    *
    * Nota: No recibe parametros.
    * @return result Resultado del query que genera la lista de todas las opciones del menú del nivel 3.*/    
    function menuNivel_3_optList()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::path()."n3_opt_select.sql",null)));
    } 

    /** Borra registro nivel 3.
    * Descripción: Borrarr el registro del nivel 3 (tres).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_3_rowDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n3_delete_pl.sql", $_POST));
	return $row[0];
    }

    /** Edita registro del menú del nivel 3.
    * Descripción: Edita registro del menú del nivel 3 (tres).
    *
    * @param array $aId Arreglo que contiene el ID del registro.
    * @return result Resultado del query que genera registro del menú del nivel 3.*/
    function menuNivel_3_rowEdit($aId)
    {
        return DbFunc::exeQryFile(self::path()."n3_row_select.sql", $aId);
    }

    /** Salva registro nivel 3.
    * Descripción: Salvar el registro del nivel 3 (tres).
    *
    * Nota: El parameto viaja por el arreglo $_POST
    * @return string Retorna el valor devuelto por el procedimiento almacenado.*/
    function menuNivel_3_rowSave()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."n3_registro_pl.sql", $_POST));
	return $row[0];
    }

    /** Hace opciones del menú.
    * Descripción: Genera toda la información para crear las opciones de menú (editar perfil del usuario).
    * @return result Devuelve toda la información para crear las opciones de menú.*/
    function menuOptMake()
    {
        $strArray0 = self::menuNivel_0_optList();        
        $strArray1 = self::menuNivel_1_optList();
        $strArray2 = self::menuNivel_2_optList();
        $strArray3 = self::menuNivel_3_optList();        
        $strArray0 = substr($strArray0,0,strlen($strArray0));
        $strArray1 = substr($strArray1,0,strlen($strArray1));
        $strArray2 = substr($strArray2,0,strlen($strArray2));
        $strArray3 = substr($strArray3,0,strlen($strArray3));      
        return '['.$strArray0.','.$strArray1.','.$strArray2.','.$strArray3.']';   
    }

    /** Lista del status del menú.
    * Descripción: Lista del status de las opciones del menú.
    * @return result Resultado del query que genera la lista del status de las opciones del menú.*/
    function menuStatusList()
    {
        return DbFunc::exeQryFile(self::path()."list_status_select.sql", null);
    }

    function menuStatusListLessActive() 
    {
        return DbFunc::exeQryFile(self::path()."list_status_less_active_select.sql", null);
    }

    function menuStatusListOnlyActive() 
    {
        return DbFunc::exeQryFile(self::path()."list_status_only_active_select.sql", null);
    }

    /** Valida Entrada del menú.
    * Descripción: Validar las entradas del menú, previniendo ataques de hacker.
    *
    * Nota: Recibe parametros por $_POST
    * @return array Devuelve $_POST validado o aborta el sistema.*/
    function menuValidateImput()
    {
	$tempo = $_POST['ruta'];
	unset($_POST['ruta']);
        $_POST['ruta'] = $tempo;
     }
       
}

