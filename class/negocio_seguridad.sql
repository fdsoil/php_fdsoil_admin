--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

-- Started on 2020-07-11 22:22:34 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 222739)
-- Name: borrar; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA borrar;


ALTER SCHEMA borrar OWNER TO postgres;

--
-- TOC entry 11 (class 2615 OID 178537)
-- Name: minuta; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA minuta;


ALTER SCHEMA minuta OWNER TO postgres;

--
-- TOC entry 8 (class 2615 OID 178539)
-- Name: seguridad; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA seguridad;


ALTER SCHEMA seguridad OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12393)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2 (class 3079 OID 178540)
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- TOC entry 306 (class 1255 OID 179108)
-- Name: minuta_acuerdo_register(integer, integer, text, character varying, text); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_acuerdo_register(i_id integer, i_id_minuta integer, i_acuerdo text, i_responsable character varying, i_observacion text) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_acuerdo(
                        id_minuta,
                        acuerdo,
                        responsable,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_acuerdo,
                        i_responsable,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_acuerdo SET
                        id_minuta=i_id_minuta,
                        acuerdo=i_acuerdo,
                        responsable=i_responsable,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_acuerdo_register(i_id integer, i_id_minuta integer, i_acuerdo text, i_responsable character varying, i_observacion text) OWNER TO postgres;

--
-- TOC entry 307 (class 1255 OID 179109)
-- Name: minuta_acuerdo_remove(integer); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_acuerdo_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_acuerdo WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_acuerdo_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 308 (class 1255 OID 179110)
-- Name: minuta_asistente_register(integer, integer, character varying, character varying, integer, integer, integer, character varying, character varying, text); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_asistente_register(i_id integer, i_id_minuta integer, i_cedula character varying, i_nombre character varying, i_id_ente integer, i_id_dependencia integer, i_id_cargo integer, i_correo character varying, i_telefono character varying, i_observacion text) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_asistente(
                        id_minuta,
                        cedula,
                        nombre,
                        id_ente,
                        id_dependencia,
                        id_cargo,
                        correo,
                        telefono,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_cedula,
                        i_nombre,
                        i_id_ente,
                        i_id_dependencia,
                        i_id_cargo,
                        i_correo,
                        i_telefono,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_asistente SET
                        id_minuta=i_id_minuta,
                        cedula=i_cedula,
                        nombre=i_nombre,
                        id_ente=i_id_ente,
                        id_dependencia=i_id_dependencia,
                        id_cargo=i_id_cargo,
                        correo=i_correo,
                        telefono=i_telefono,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_asistente_register(i_id integer, i_id_minuta integer, i_cedula character varying, i_nombre character varying, i_id_ente integer, i_id_dependencia integer, i_id_cargo integer, i_correo character varying, i_telefono character varying, i_observacion text) OWNER TO postgres;

--
-- TOC entry 309 (class 1255 OID 179111)
-- Name: minuta_asistente_remove(integer); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_asistente_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_asistente WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_asistente_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 310 (class 1255 OID 179112)
-- Name: minuta_planteamiento_register(integer, integer, text, character varying, text); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_planteamiento_register(i_id integer, i_id_minuta integer, i_planteamiento text, i_ponente character varying, i_observacion text) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_planteamiento(
                        id_minuta,
                        planteamiento,
                        ponente,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_planteamiento,
                        i_ponente,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_planteamiento SET
                        id_minuta=i_id_minuta,
                        planteamiento=i_planteamiento,
                        ponente=i_ponente,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_planteamiento_register(i_id integer, i_id_minuta integer, i_planteamiento text, i_ponente character varying, i_observacion text) OWNER TO postgres;

--
-- TOC entry 311 (class 1255 OID 179113)
-- Name: minuta_planteamiento_remove(integer); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.minuta_planteamiento_remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_planteamiento WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.minuta_planteamiento_remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 304 (class 1255 OID 179106)
-- Name: register(integer, integer, date, time without time zone, character varying, integer, integer, character varying, text, text); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.register(i_id integer, i_id_ciudad integer, i_fecha date, i_hora time without time zone, i_lugar character varying, i_id_ente integer, i_id_dependencia integer, i_asunto character varying, i_motivo text, i_observacion text) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM minuta.minuta
                        WHERE id_ciudad=i_id_ciudad
                                        AND fecha=i_fecha
                                        AND hora=i_hora
                                        AND asunto=i_asunto;
                IF  v_existe='f' THEN
                        INSERT INTO minuta.minuta(
                                id_ciudad,
                                fecha,
                                hora,
                                lugar,
                                id_ente,
                                id_dependencia,
                                asunto,
                                motivo,
                                observacion)
                        VALUES (
                                i_id_ciudad,
                                i_fecha,
                                i_hora,
                                i_lugar,
                                i_id_ente,
                                i_id_dependencia,
                                i_asunto,
                                i_motivo,
                                i_observacion);
                        SELECT max(id) INTO v_id FROM minuta.minuta
                                WHERE id_ciudad=i_id_ciudad
                                        AND fecha=i_fecha
                                        AND hora=i_hora
                                        AND asunto=i_asunto;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE minuta.minuta SET
                        lugar=i_lugar,
                        id_ente=i_id_ente,
                        id_dependencia=i_id_dependencia,
                        motivo=i_motivo,
                        observacion=i_observacion
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.register(i_id integer, i_id_ciudad integer, i_fecha date, i_hora time without time zone, i_lugar character varying, i_id_ente integer, i_id_dependencia integer, i_asunto character varying, i_motivo text, i_observacion text) OWNER TO postgres;

--
-- TOC entry 305 (class 1255 OID 179107)
-- Name: remove(integer); Type: FUNCTION; Schema: minuta; Owner: postgres
--

CREATE FUNCTION minuta.remove(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$$;


ALTER FUNCTION minuta.remove(i_id integer) OWNER TO postgres;

--
-- TOC entry 313 (class 1255 OID 222740)
-- Name: generar_numero(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.generar_numero() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
                   NEW.reposicion_num:= lpad(CAST ((SELECT count (id) FROM public.reposicion WHERE (EXTRACT(YEAR FROM now())) = EXTRACT(YEAR FROM reposicion_fec)
                   )+1 AS CHARACTER VARYING),7,'0') || '' ||
       lpad(CAST ((SELECT count (id) FROM public.reposicion WHERE (EXTRACT(MONTH FROM now())) = EXTRACT(MONTH FROM reposicion_fec)
                   )+1 AS CHARACTER VARYING),6,'0');
                   RETURN NEW;
               END;$$;


ALTER FUNCTION public.generar_numero() OWNER TO postgres;

--
-- TOC entry 312 (class 1255 OID 178601)
-- Name: auditoria_general(); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.auditoria_general() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
 DECLARE 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
 --  @propiedad:MPPComercio 
 --  @Autor: Gregorio Bolívar 
 --  @email: elalconxvii@gmail.com 
 --  @Fecha de Creacion: 20/05/2015 
 --  @Auditado por: Gregorio J Bolívar B 
 --  @Fecha de Modificacion: 22/05/2015
 --  @Descripción: Funcion de auditoria para todos los sistemas, con el fin de contemplar todos los procesos efectuados por los usuarios dentro del sistema. 
 --  @version: 0.6 
 --  @Blog: http://gbbolivar.wordpress.com/ 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Variables de configuracion para la conexion con el dblink
   db_audi VARCHAR :='negocio_auditoria'; 
   ip_audi VARCHAR :='127.0.0.1'; 
   es_audi VARCHAR :='postgres'; 
   ps_audi VARCHAR :='postgres';
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Usuario de base de datos, es requerido que este usuario sea diferente a u_producto
   user_access VARCHAR := user;
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Definicion de los campos relacionados al identificador del usuario, es recomendable que toda tabla tiene que tener identificador del usuario, cuando sea insert o update
   user_id_insert VARCHAR ;
   user_id_otros  VARCHAR ;
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Esta variable es para concatenar la conexion de los valores
   conf TEXT :='';
   sqla TEXT :='';
 BEGIN
   conf='hostaddr='''||ip_audi||''' dbname='''||db_audi||''' user='''||es_audi||''' password='''||ps_audi ||'''';
 IF(TG_OP = 'INSERT')THEN
     user_id_insert = NEW.id_user_insert;
     IF(CURRENT_USER=user_access)THEN
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, new_values, entidad_id, fecha) values ('''||user_id_insert||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||NEW.id||''' , '''|| now() ||''')';                                      
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, new_values, entidad_id, fecha) values ('''|| inet_client_addr() ||''', '''|| USER ||''', '''|| current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||NEW.id||''' , '''|| now() ||''')';                                                                             
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
 ELSE 
     user_id_otros   = OLD.id_user_update; 
     IF(TG_OP = 'UPDATE')THEN 
        IF(CURRENT_USER=user_access)THEN                                                                                                                                                                                                                                                                                                                                                                                                                
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, new_values, old_values, entidad_id, fecha) values ('''||user_id_otros||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||OLD||''' , '''||OLD.id||''' , '''|| now() ||''')';           
        ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, new_values, old_values, entidad_id, fecha) values ('''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||OLD||''' , '''||OLD.id||''' , '''|| now() ||''')';                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
        END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
      END IF; 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
      IF(TG_OP = 'DELETE')THEN 
         IF(CURRENT_USER=user_access)THEN                                                                                                                                                                                                                                                                                                                                                                                                                
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, old_values, entidad_id, fecha) values ('''||user_id_otros||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||OLD||''' , '''||OLD.id||''' , '||' now() '||')';                                         
         ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, old_values, entidad_id, fecha) values ('''||inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||OLD||''' , '''||OLD.id||''' , '||' now() '||')';                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
      END IF; 
 END IF; 
      PERFORM dblink_exec(''||conf||'',''||sqla||''); 
      return new; 
 END; 
 $$;


ALTER FUNCTION seguridad.auditoria_general() OWNER TO postgres;

--
-- TOC entry 285 (class 1255 OID 178602)
-- Name: config_dblink(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.config_dblink(i_id integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE 
    o_return character varying;
BEGIN
        o_return:='';
        SELECT  'host='||servidor||' port='||puerto||' dbname='||nbd||' user='||usuario||' password='||clave 

     
        INTO o_return FROM seguridad.config_dblink WHERE id=i_id;   

  RETURN o_return; 
END;
$$;


ALTER FUNCTION seguridad.config_dblink(i_id integer) OWNER TO postgres;

--
-- TOC entry 286 (class 1255 OID 178603)
-- Name: dblink_clave(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.dblink_clave(i_id integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE  
	o_return character varying;
BEGIN
		o_return:='';
		SELECT  'dbname='||dbname||' port='||puerto||' user='||usuario||' password='||clave  
		INTO o_return FROM seguridad.config_dblink WHERE id=i_id;	

  RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.dblink_clave(i_id integer) OWNER TO postgres;

--
-- TOC entry 287 (class 1255 OID 178604)
-- Name: menu_0_delete(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_0_delete(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	SELECT CASE WHEN count(id_menu_0)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_1 WHERE id_menu_0=i_id;
	IF v_existe='f' THEN
		DELETE FROM seguridad.menu_0 WHERE id=i_id;
		o_return:='B';
	ELSE
		o_return:='U';
	END IF;	
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_0_delete(i_id integer) OWNER TO postgres;

--
-- TOC entry 288 (class 1255 OID 178605)
-- Name: menu_0_registrar(character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_0_registrar(i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_id='' THEN 
		SELECT CASE WHEN count(titulo)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_0 WHERE titulo=i_opcion;
		IF v_existe='f' THEN	
			INSERT INTO seguridad.menu_0(titulo, ruta, orden, id_status) 
			VALUES (i_opcion, i_ruta, i_orden, i_id_status);
			o_return:='C'; 
		ELSE
			o_return:='T';
		END IF;
	ELSE
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe 
		FROM seguridad.menu_0 WHERE id=CAST(i_id AS integer);
		IF v_existe='t' THEN
			UPDATE seguridad.menu_0 
			SET titulo=i_opcion, ruta=i_ruta, orden=i_orden, id_status=i_id_status 
			WHERE id = CAST(i_id AS integer);
			o_return:='A';	
		ELSE
			o_return:='R';
		END IF;		
	END IF;
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_0_registrar(i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) OWNER TO postgres;

--
-- TOC entry 289 (class 1255 OID 178606)
-- Name: menu_1_delete(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_1_delete(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	SELECT CASE WHEN count(id_menu_1)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_2 WHERE id_menu_1=i_id;
	IF v_existe='f' THEN
		DELETE FROM seguridad.menu_1 WHERE id=i_id;
		o_return:='B';
	ELSE
		o_return:='U';
	END IF;	
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_1_delete(i_id integer) OWNER TO postgres;

--
-- TOC entry 290 (class 1255 OID 178607)
-- Name: menu_1_registrar(integer, character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_1_registrar(id_n0 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_id='' THEN 
		SELECT CASE WHEN count(titulo)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_1 WHERE titulo=i_opcion;
		IF v_existe='f' THEN	
			INSERT INTO seguridad.menu_1(id_menu_0, titulo, ruta, orden, id_status) 
			VALUES (id_n0, i_opcion, i_ruta, i_orden, i_id_status);
			o_return:='C'; 
		ELSE
			o_return:='T';
		END IF;
	ELSE
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe 
		FROM seguridad.menu_1 WHERE id=CAST(i_id AS integer);
		IF v_existe='t' THEN
			UPDATE seguridad.menu_1 
			SET id_menu_0=id_n0, titulo=i_opcion, ruta=i_ruta, orden=i_orden, id_status=i_id_status 
			WHERE id = CAST(i_id AS integer);
			o_return:='A';	
		ELSE
			o_return:='R';
		END IF;		
	END IF;
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_1_registrar(id_n0 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) OWNER TO postgres;

--
-- TOC entry 291 (class 1255 OID 178608)
-- Name: menu_2_delete(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_2_delete(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	SELECT CASE WHEN count(id_menu_2)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_3 WHERE id_menu_2=i_id;
	IF v_existe='f' THEN
		DELETE FROM seguridad.menu_2 WHERE id=i_id;
		o_return:='B';
	ELSE
		o_return:='U';
	END IF;	
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_2_delete(i_id integer) OWNER TO postgres;

--
-- TOC entry 292 (class 1255 OID 178609)
-- Name: menu_2_registrar(integer, character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_2_registrar(id_n1 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_id='' THEN 
		SELECT CASE WHEN count(titulo)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_2 WHERE titulo=i_opcion;
		IF v_existe='f' THEN	
			INSERT INTO seguridad.menu_2(id_menu_1, titulo, ruta, orden, id_status) 
			VALUES (id_n1, i_opcion, i_ruta, i_orden, i_id_status);
			o_return:='C'; 
		ELSE
			o_return:='T';
		END IF;
	ELSE
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe 
		FROM seguridad.menu_2 WHERE id=CAST(i_id AS integer);
		IF v_existe='t' THEN
			UPDATE seguridad.menu_2 
			SET id_menu_1=id_n1, titulo=i_opcion, ruta=i_ruta, orden=i_orden, id_status=i_id_status 
			WHERE id = CAST(i_id AS integer);
			o_return:='A';	
		ELSE
			o_return:='R';
		END IF;		
	END IF;
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_2_registrar(id_n1 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) OWNER TO postgres;

--
-- TOC entry 293 (class 1255 OID 178610)
-- Name: menu_3_delete(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_3_delete(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';

	--SELECT CASE WHEN count(id_menu_2)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_3 WHERE id_menu_2=i_id;
	--IF v_existe='f' THEN
		DELETE FROM seguridad.menu_3 WHERE id=i_id;
		o_return:='B';
	--ELSE
	--	o_return:='U';
	--END IF;
	
	
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_3_delete(i_id integer) OWNER TO postgres;

--
-- TOC entry 294 (class 1255 OID 178611)
-- Name: menu_3_registrar(integer, character varying, character varying, character varying, integer, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.menu_3_registrar(id_n2 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_id='' THEN 
		SELECT CASE WHEN count(titulo)=0 THEN false ELSE true END INTO v_existe FROM seguridad.menu_3 WHERE titulo=i_opcion;
		IF v_existe='f' THEN	
			INSERT INTO seguridad.menu_3(id_menu_2, titulo, ruta, orden, id_status) 
			VALUES (id_n2, i_opcion, i_ruta, i_orden, i_id_status);
			o_return:='C'; 
		ELSE
			o_return:='T';
		END IF;
	ELSE
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe 
		FROM seguridad.menu_3 WHERE id=CAST(i_id AS integer);
		IF v_existe='t' THEN
			UPDATE seguridad.menu_3 
			SET id_menu_2=id_n2, titulo=i_opcion, ruta=i_ruta, orden=i_orden, id_status=i_id_status 
			WHERE id = CAST(i_id AS integer);
			o_return:='A';	
		ELSE
			o_return:='R';
		END IF;		
	END IF;
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.menu_3_registrar(id_n2 integer, i_id character varying, i_opcion character varying, i_ruta character varying, i_orden integer, i_id_status integer) OWNER TO postgres;

--
-- TOC entry 295 (class 1255 OID 178612)
-- Name: rol_0_register(integer, character varying); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_0_register(i_rol integer, i_cadena character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE            
      v_arreglo1 character varying[]; 
BEGIN
	SELECT string_to_array(i_cadena, '¬') INTO v_arreglo1;
	FOR i IN array_lower(v_arreglo1, 1) .. array_upper(v_arreglo1, 1) LOOP
		INSERT INTO seguridad.rol_0( id_rol, id_menu_0) VALUES ( i_rol, cast(v_arreglo1[i] as integer));
	END LOOP;
  RETURN 'A';  
END;
$$;


ALTER FUNCTION seguridad.rol_0_register(i_rol integer, i_cadena character varying) OWNER TO postgres;

--
-- TOC entry 296 (class 1255 OID 178613)
-- Name: rol_1_register(integer, character varying); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_1_register(i_rol integer, i_cadena character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE            
      v_arreglo1 character varying[]; 
      v_arreglo2 character varying[]; 
BEGIN
	SELECT string_to_array(i_cadena, '¬') INTO v_arreglo1;
	FOR i IN array_lower(v_arreglo1, 1) .. array_upper(v_arreglo1, 1) LOOP
		SELECT string_to_array(v_arreglo1[i], '|') INTO v_arreglo2;
		INSERT INTO seguridad.rol_1( id_rol, id_menu_0, id_menu_1)
		VALUES ( i_rol, cast(v_arreglo2[1] as integer), cast(v_arreglo2[2] as integer));
	END LOOP;
  RETURN 'A';
  
END;
$$;


ALTER FUNCTION seguridad.rol_1_register(i_rol integer, i_cadena character varying) OWNER TO postgres;

--
-- TOC entry 297 (class 1255 OID 178614)
-- Name: rol_2_register(integer, character varying); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_2_register(i_rol integer, i_cadena character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE            
      v_arreglo1 character varying[]; 
      v_arreglo2 character varying[]; 
BEGIN
	SELECT string_to_array(i_cadena, '¬') INTO v_arreglo1;
	FOR i IN array_lower(v_arreglo1, 1) .. array_upper(v_arreglo1, 1) LOOP
		SELECT string_to_array(v_arreglo1[i], '|') INTO v_arreglo2;
		INSERT INTO seguridad.rol_2( id_rol, id_menu_1, id_menu_2)
		VALUES ( i_rol, cast(v_arreglo2[1] as integer), cast(v_arreglo2[2] as integer));
	END LOOP;
  RETURN 'A';  
END;
$$;


ALTER FUNCTION seguridad.rol_2_register(i_rol integer, i_cadena character varying) OWNER TO postgres;

--
-- TOC entry 298 (class 1255 OID 178615)
-- Name: rol_3_register(integer, character varying); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_3_register(i_rol integer, i_cadena character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE            
      v_arreglo1 character varying[]; 
      v_arreglo2 character varying[]; 
BEGIN
	SELECT string_to_array(i_cadena, '¬') INTO v_arreglo1;
	FOR i IN array_lower(v_arreglo1, 1) .. array_upper(v_arreglo1, 1) LOOP
		SELECT string_to_array(v_arreglo1[i], '|') INTO v_arreglo2;
		INSERT INTO seguridad.rol_3( id_rol, id_menu_2, id_menu_3)
		VALUES ( i_rol, cast(v_arreglo2[1] as integer), cast(v_arreglo2[2] as integer));
	END LOOP;
  RETURN 'A';
  
END;
$$;


ALTER FUNCTION seguridad.rol_3_register(i_rol integer, i_cadena character varying) OWNER TO postgres;

--
-- TOC entry 299 (class 1255 OID 178616)
-- Name: rol_delete(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_delete(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	o_return:='';

	SELECT seguridad.rol_delete_aux(i_id) INTO o_return;
	IF o_return = 'B' THEN
		DELETE FROM seguridad.roles WHERE id=i_id;
		o_return:='B';
	END IF;
	
	RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.rol_delete(i_id integer) OWNER TO postgres;

--
-- TOC entry 300 (class 1255 OID 178617)
-- Name: rol_delete_aux(integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_delete_aux(i_id integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_existe boolean;
BEGIN
	DELETE FROM seguridad.rol_3 WHERE id_rol=i_id;
	DELETE FROM seguridad.rol_2 WHERE id_rol=i_id;
	DELETE FROM seguridad.rol_1 WHERE id_rol=i_id;
	DELETE FROM seguridad.rol_0 WHERE id_rol=i_id;
	RETURN 'B';  
END;
$$;


ALTER FUNCTION seguridad.rol_delete_aux(i_id integer) OWNER TO postgres;

--
-- TOC entry 301 (class 1255 OID 178618)
-- Name: rol_register(integer, character varying, character varying, character varying, character varying, character varying, character varying, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.rol_register(i_rol integer, i_descripcion character varying, i_pag_ini_default character varying, i_cadena_0 character varying, i_cadena_1 character varying, i_cadena_2 character varying, i_cadena_3 character varying, i_status integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE   
	o_return character;
	v_id  integer;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_rol=0 THEN 
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.roles WHERE descripcion=i_descripcion;
		IF v_existe='f' THEN
			INSERT INTO seguridad.roles( descripcion, id_status, pag_ini_default) VALUES ( i_descripcion, i_status, i_pag_ini_default);		
			SELECT id INTO v_id FROM  seguridad.roles WHERE descripcion=i_descripcion;			
			o_return:='C';	
		ELSE 
			o_return:='T';		
		END IF;
	ELSE	
		v_id:=i_rol;
		UPDATE seguridad.roles SET descripcion=i_descripcion, id_status=i_status, pag_ini_default=i_pag_ini_default WHERE id=v_id;
		DELETE FROM seguridad.rol_3 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_2 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_1 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_0 WHERE id_rol=v_id;		
		o_return:='A';			
	END IF;
	IF i_cadena_0 !='' AND (o_return = 'C' OR o_return = 'A') THEN			
		SELECT seguridad.rol_0_register(v_id, i_cadena_0) INTO o_return; 
		IF i_cadena_1 !='' AND o_return = 'A' THEN
			SELECT seguridad.rol_1_register(v_id, i_cadena_1) INTO o_return; 
			IF i_cadena_2 !='' AND o_return = 'A' THEN
				SELECT seguridad.rol_2_register(v_id, i_cadena_2) INTO o_return; 
				IF i_cadena_3 !='' AND o_return = 'A' THEN
					SELECT seguridad.rol_3_register(v_id, i_cadena_3) INTO o_return; 
				END IF;							
			END IF;					
		END IF;				
	END IF;
RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.rol_register(i_rol integer, i_descripcion character varying, i_pag_ini_default character varying, i_cadena_0 character varying, i_cadena_1 character varying, i_cadena_2 character varying, i_cadena_3 character varying, i_status integer) OWNER TO postgres;

--
-- TOC entry 302 (class 1255 OID 178619)
-- Name: usuario_registrar(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.usuario_registrar(i_id_usuario integer, i_usuario character varying, i_correo character varying, i_cedula character varying, i_clave character varying, i_nombre character varying, i_apellido character varying, i_celular character varying, i_telefono1 character varying, i_telefono2 character varying, i_id_rol integer, i_id_status integer, i_pregunta_seguridad character varying, i_respuesta_seguridad character varying, i_id_user_action integer) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE 
        o_return character;
        v_existe boolean;
BEGIN
        o_return:='';
        IF  i_id_usuario=0 THEN 
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.usuario WHERE cedula = i_cedula;
                IF  v_existe='f' THEN
			INSERT INTO seguridad.usuario(
                            usuario, 
                            correo, 
                            cedula, 
                            clave, 
                            nombre, 
                            apellido, 
                            celular,
			    telefono1,
                            telefono2,
                            id_rol,
                            id_status,
                            pregunta_seguridad,
			    respuesta_seguridad,
                            fecha_registro,
                            id_user_insert,
                            id_user_update
                        ) VALUES (
                            i_usuario,
                            i_correo,
                            i_cedula,
                            i_clave,
                            i_nombre,
                            i_apellido,
                            i_celular,
			    i_telefono1,
                            i_telefono2,
                            i_id_rol,
                            i_id_status,
                            i_pregunta_seguridad,
			    i_respuesta_seguridad,
                            current_date,
                            i_id_user_action,
                            i_id_user_action
                        );
			o_return:= 'C';
		ELSE
                        o_return:='T';        
		END IF;
	ELSE
		UPDATE seguridad.usuario
			SET usuario = i_usuario, 
                            correo = i_correo,
                            cedula = i_cedula,
                            nombre = i_nombre,
                            apellido = i_apellido, 
			    celular = i_celular,
                            telefono1 = i_telefono1,
                            telefono2 = i_telefono2,
                            id_rol = i_id_rol,
                            id_status = i_id_status, 
			    pregunta_seguridad = i_pregunta_seguridad, 
                            respuesta_seguridad = i_respuesta_seguridad,
                            fecha_registro=current_date,
                            fecha_update=(SELECT ('now'::text)::date),
                            hora_update=(SELECT ('now'::text)::time),
                            id_user_update = i_id_user_action
		WHERE id=i_id_usuario;		
		o_return:='A';	
	END IF;
        RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.usuario_registrar(i_id_usuario integer, i_usuario character varying, i_correo character varying, i_cedula character varying, i_clave character varying, i_nombre character varying, i_apellido character varying, i_celular character varying, i_telefono1 character varying, i_telefono2 character varying, i_id_rol integer, i_id_status integer, i_pregunta_seguridad character varying, i_respuesta_seguridad character varying, i_id_user_action integer) OWNER TO postgres;

--
-- TOC entry 303 (class 1255 OID 178620)
-- Name: usuario_registrar_web(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying); Type: FUNCTION; Schema: seguridad; Owner: postgres
--

CREATE FUNCTION seguridad.usuario_registrar_web(i_usuario character varying, i_correo character varying, i_cedula character varying, i_clave character varying, i_nombre character varying, i_apellido character varying, i_celular character varying, i_telefono1 character varying, i_telefono2 character varying, i_id_rol integer, i_id_status integer, i_pregunta_seguridad character varying, i_respuesta_seguridad character varying) RETURNS character
    LANGUAGE plpgsql
    AS $$
DECLARE 
        o_return character;
        v_existe boolean;
        oCORREO  integer;
BEGIN
        o_return:='';
        SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.usuario WHERE cedula = i_cedula;        
        IF  v_existe='f' THEN
		SELECT count(correo) INTO oCORREO FROM seguridad.usuario WHERE correo = i_correo;  
		IF (oCORREO) > 0 THEN 
			o_return:= 'S';
		ELSE
			INSERT INTO seguridad.usuario(usuario, correo, cedula, clave, nombre, apellido, celular,
			telefono1, telefono2, id_rol, id_status, pregunta_seguridad,
			respuesta_seguridad, fecha_registro, id_user_insert, id_user_update)
			VALUES (i_usuario, i_correo, i_cedula, i_clave, i_nombre, i_apellido, i_celular,
			i_telefono1, i_telefono2, i_id_rol, i_id_status, i_pregunta_seguridad,
			i_respuesta_seguridad, current_date, 0, 0);
			o_return:= 'C';
                END IF;
                
        ELSE
		o_return:='T';        
        END IF;
        RETURN o_return;  
END;
$$;


ALTER FUNCTION seguridad.usuario_registrar_web(i_usuario character varying, i_correo character varying, i_cedula character varying, i_clave character varying, i_nombre character varying, i_apellido character varying, i_celular character varying, i_telefono1 character varying, i_telefono2 character varying, i_id_rol integer, i_id_status integer, i_pregunta_seguridad character varying, i_respuesta_seguridad character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 190 (class 1259 OID 178641)
-- Name: cargo; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.cargo (
    id integer NOT NULL,
    descripcion character varying
);


ALTER TABLE minuta.cargo OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 178647)
-- Name: ciudad; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.ciudad (
    id integer NOT NULL,
    descripcion character varying
);


ALTER TABLE minuta.ciudad OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 178653)
-- Name: dependencia; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.dependencia (
    id integer NOT NULL,
    descripcion character varying
);


ALTER TABLE minuta.dependencia OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 178659)
-- Name: ente; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.ente (
    id integer NOT NULL,
    descripcion character varying
);


ALTER TABLE minuta.ente OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 178665)
-- Name: minuta; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.minuta (
    id integer NOT NULL,
    id_ciudad integer,
    fecha date,
    hora time without time zone,
    lugar character varying,
    id_ente integer,
    id_dependencia integer,
    asunto character varying,
    motivo text,
    observacion text
);


ALTER TABLE minuta.minuta OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 178671)
-- Name: minuta_acuerdo; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.minuta_acuerdo (
    id integer NOT NULL,
    id_minuta integer,
    acuerdo text,
    responsable character varying,
    observacion text DEFAULT ''::text
);


ALTER TABLE minuta.minuta_acuerdo OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 178678)
-- Name: minuta_acuerdo_id_seq; Type: SEQUENCE; Schema: minuta; Owner: postgres
--

CREATE SEQUENCE minuta.minuta_acuerdo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE minuta.minuta_acuerdo_id_seq OWNER TO postgres;

--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 196
-- Name: minuta_acuerdo_id_seq; Type: SEQUENCE OWNED BY; Schema: minuta; Owner: postgres
--

ALTER SEQUENCE minuta.minuta_acuerdo_id_seq OWNED BY minuta.minuta_acuerdo.id;


--
-- TOC entry 197 (class 1259 OID 178680)
-- Name: minuta_asistente; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.minuta_asistente (
    id integer NOT NULL,
    id_minuta integer,
    cedula character varying,
    nombre character varying,
    id_ente integer,
    id_dependencia integer,
    id_cargo integer,
    correo character varying,
    telefono character varying,
    observacion text DEFAULT ''::text
);


ALTER TABLE minuta.minuta_asistente OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 178687)
-- Name: minuta_asistente_id_seq; Type: SEQUENCE; Schema: minuta; Owner: postgres
--

CREATE SEQUENCE minuta.minuta_asistente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE minuta.minuta_asistente_id_seq OWNER TO postgres;

--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 198
-- Name: minuta_asistente_id_seq; Type: SEQUENCE OWNED BY; Schema: minuta; Owner: postgres
--

ALTER SEQUENCE minuta.minuta_asistente_id_seq OWNED BY minuta.minuta_asistente.id;


--
-- TOC entry 199 (class 1259 OID 178689)
-- Name: minuta_id_seq; Type: SEQUENCE; Schema: minuta; Owner: postgres
--

CREATE SEQUENCE minuta.minuta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE minuta.minuta_id_seq OWNER TO postgres;

--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 199
-- Name: minuta_id_seq; Type: SEQUENCE OWNED BY; Schema: minuta; Owner: postgres
--

ALTER SEQUENCE minuta.minuta_id_seq OWNED BY minuta.minuta.id;


--
-- TOC entry 200 (class 1259 OID 178691)
-- Name: minuta_planteamiento; Type: TABLE; Schema: minuta; Owner: postgres
--

CREATE TABLE minuta.minuta_planteamiento (
    id integer NOT NULL,
    id_minuta integer,
    planteamiento text,
    ponente character varying,
    observacion text DEFAULT ''::text
);


ALTER TABLE minuta.minuta_planteamiento OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 178698)
-- Name: minuta_planteamiento_id_seq; Type: SEQUENCE; Schema: minuta; Owner: postgres
--

CREATE SEQUENCE minuta.minuta_planteamiento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE minuta.minuta_planteamiento_id_seq OWNER TO postgres;

--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 201
-- Name: minuta_planteamiento_id_seq; Type: SEQUENCE OWNED BY; Schema: minuta; Owner: postgres
--

ALTER SEQUENCE minuta.minuta_planteamiento_id_seq OWNED BY minuta.minuta_planteamiento.id;


--
-- TOC entry 229 (class 1259 OID 222744)
-- Name: reposicion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reposicion (
    id integer NOT NULL,
    reposicion_num character varying(27) NOT NULL,
    reposicion_fec date DEFAULT ('now'::text)::date NOT NULL,
    sujeto character varying(40) NOT NULL,
    descripcion text NOT NULL,
    observacion text NOT NULL,
    entregada_fec date,
    id_user_insert integer DEFAULT 1 NOT NULL,
    id_user_update integer DEFAULT 1 NOT NULL,
    id_user_edit integer DEFAULT 1 NOT NULL,
    editing integer DEFAULT 1 NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.reposicion OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 222769)
-- Name: reposicion_aux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reposicion_aux (
    id integer NOT NULL,
    id_reposicion integer NOT NULL,
    id_presentacion integer NOT NULL,
    solicitada_cant integer DEFAULT 0 NOT NULL,
    entregada_cant integer DEFAULT 0 NOT NULL,
    entregada_fec date,
    id_user_insert integer NOT NULL,
    id_user_update integer NOT NULL,
    date_insert date DEFAULT ('now'::text)::date NOT NULL,
    date_update date DEFAULT ('now'::text)::date NOT NULL,
    time_insert time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    time_update time(0) without time zone DEFAULT ('now'::text)::time without time zone NOT NULL
);


ALTER TABLE public.reposicion_aux OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 222767)
-- Name: reposicion_aux_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reposicion_aux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reposicion_aux_id_seq OWNER TO postgres;

--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 230
-- Name: reposicion_aux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reposicion_aux_id_seq OWNED BY public.reposicion_aux.id;


--
-- TOC entry 202 (class 1259 OID 178763)
-- Name: seq_auditoria; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_auditoria
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_auditoria OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 178765)
-- Name: auditoria; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.auditoria (
    id integer DEFAULT nextval('seguridad.seq_auditoria'::regclass) NOT NULL,
    remote_addr character varying(20),
    remote_port integer,
    id_usuario integer NOT NULL,
    script_filename character varying(500),
    reqs_method character varying(15),
    query text,
    f_serv_ap date,
    h_serv_ap time without time zone,
    f_serv_bd date DEFAULT ('now'::text)::date,
    h_serv_bd time without time zone DEFAULT ('now'::text)::time without time zone,
    coment text
);


ALTER TABLE seguridad.auditoria OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 206249)
-- Name: config_dblink; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.config_dblink (
    id integer NOT NULL,
    servidor character varying,
    puerto character varying,
    nbd character varying,
    usuario character varying,
    clave character varying
);


ALTER TABLE seguridad.config_dblink OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 178777)
-- Name: seq_menu_0; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_menu_0
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_menu_0 OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 178779)
-- Name: menu_0; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.menu_0 (
    id integer DEFAULT nextval('seguridad.seq_menu_0'::regclass) NOT NULL,
    titulo character varying(50),
    ruta character varying(150),
    orden integer,
    id_status integer DEFAULT 1
);


ALTER TABLE seguridad.menu_0 OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 178784)
-- Name: seq_menu_1; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_menu_1
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_menu_1 OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 178786)
-- Name: menu_1; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.menu_1 (
    id integer DEFAULT nextval('seguridad.seq_menu_1'::regclass) NOT NULL,
    id_menu_0 integer,
    titulo character varying(50),
    ruta character varying(150),
    orden integer,
    id_status integer
);


ALTER TABLE seguridad.menu_1 OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 178790)
-- Name: seq_menu_2; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_menu_2
    START WITH 20
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_menu_2 OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 178792)
-- Name: menu_2; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.menu_2 (
    id integer DEFAULT nextval('seguridad.seq_menu_2'::regclass) NOT NULL,
    id_menu_1 integer,
    titulo character varying(50),
    ruta character varying(150),
    orden integer,
    id_status integer
);


ALTER TABLE seguridad.menu_2 OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 178796)
-- Name: seq_menu_3; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_menu_3
    START WITH 49
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_menu_3 OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 178798)
-- Name: menu_3; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.menu_3 (
    id integer DEFAULT nextval('seguridad.seq_menu_3'::regclass) NOT NULL,
    id_menu_2 integer,
    titulo character varying(50),
    ruta character varying(150),
    orden integer,
    id_status integer
);


ALTER TABLE seguridad.menu_3 OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 178802)
-- Name: msj; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.msj (
    id integer NOT NULL,
    descripcion character varying(100)
);


ALTER TABLE seguridad.msj OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 178805)
-- Name: seq_pregunta; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_pregunta
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_pregunta OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 178807)
-- Name: pregunta_seguridad; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.pregunta_seguridad (
    id integer DEFAULT nextval('seguridad.seq_pregunta'::regclass) NOT NULL,
    descripcion character varying
);


ALTER TABLE seguridad.pregunta_seguridad OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 178814)
-- Name: seq_rol_0; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_rol_0
    START WITH 9
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_rol_0 OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 178816)
-- Name: rol_0; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.rol_0 (
    id integer DEFAULT nextval('seguridad.seq_rol_0'::regclass) NOT NULL,
    id_rol integer,
    id_menu_0 integer
);


ALTER TABLE seguridad.rol_0 OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 178820)
-- Name: seq_rol_1; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_rol_1
    START WITH 198
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_rol_1 OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 178822)
-- Name: rol_1; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.rol_1 (
    id integer DEFAULT nextval('seguridad.seq_rol_1'::regclass) NOT NULL,
    id_rol integer NOT NULL,
    id_menu_0 integer NOT NULL,
    id_menu_1 integer NOT NULL
);


ALTER TABLE seguridad.rol_1 OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 178826)
-- Name: seq_rol_2; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_rol_2
    START WITH 82
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_rol_2 OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 178828)
-- Name: rol_2; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.rol_2 (
    id integer DEFAULT nextval('seguridad.seq_rol_2'::regclass) NOT NULL,
    id_rol integer NOT NULL,
    id_menu_1 integer NOT NULL,
    id_menu_2 integer NOT NULL
);


ALTER TABLE seguridad.rol_2 OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 178832)
-- Name: seq_rol_3; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_rol_3
    START WITH 30
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_rol_3 OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 178834)
-- Name: rol_3; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.rol_3 (
    id integer DEFAULT nextval('seguridad.seq_rol_3'::regclass) NOT NULL,
    id_rol integer NOT NULL,
    id_menu_2 integer NOT NULL,
    id_menu_3 integer NOT NULL
);


ALTER TABLE seguridad.rol_3 OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 178838)
-- Name: seq_rol; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_rol
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_rol OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 178840)
-- Name: roles; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.roles (
    id integer DEFAULT nextval('seguridad.seq_rol'::regclass) NOT NULL,
    descripcion character varying(100) NOT NULL,
    id_status integer NOT NULL,
    pag_ini_default character varying(150) DEFAULT '../../../FDSoil/admin_inicio/'::character varying NOT NULL
);


ALTER TABLE seguridad.roles OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 178845)
-- Name: seq_usuario; Type: SEQUENCE; Schema: seguridad; Owner: postgres
--

CREATE SEQUENCE seguridad.seq_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguridad.seq_usuario OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 178847)
-- Name: status; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.status (
    id integer NOT NULL,
    descripcion character varying(15)
);


ALTER TABLE seguridad.status OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 178850)
-- Name: usuario; Type: TABLE; Schema: seguridad; Owner: postgres
--

CREATE TABLE seguridad.usuario (
    id integer DEFAULT nextval('seguridad.seq_usuario'::regclass) NOT NULL,
    usuario character varying(20) NOT NULL,
    correo character varying(100) NOT NULL,
    cedula character varying(15) NOT NULL,
    clave character varying(100),
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    celular character varying(20),
    telefono1 character varying(20),
    telefono2 character varying(20),
    id_rol integer NOT NULL,
    id_status integer NOT NULL,
    pregunta_seguridad character varying,
    respuesta_seguridad character varying,
    fecha_registro date,
    status_key boolean DEFAULT false NOT NULL,
    fecha_key date DEFAULT ('now'::text)::date NOT NULL,
    fecha_insert date DEFAULT ('now'::text)::date NOT NULL,
    hora_insert time without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    fecha_update date DEFAULT ('now'::text)::date NOT NULL,
    hora_update time without time zone DEFAULT ('now'::text)::time without time zone NOT NULL,
    id_user_insert integer NOT NULL,
    id_user_update integer NOT NULL
);


ALTER TABLE seguridad.usuario OWNER TO postgres;

--
-- TOC entry 2225 (class 2604 OID 178865)
-- Name: minuta id; Type: DEFAULT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta ALTER COLUMN id SET DEFAULT nextval('minuta.minuta_id_seq'::regclass);


--
-- TOC entry 2227 (class 2604 OID 178866)
-- Name: minuta_acuerdo id; Type: DEFAULT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_acuerdo ALTER COLUMN id SET DEFAULT nextval('minuta.minuta_acuerdo_id_seq'::regclass);


--
-- TOC entry 2229 (class 2604 OID 178867)
-- Name: minuta_asistente id; Type: DEFAULT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente ALTER COLUMN id SET DEFAULT nextval('minuta.minuta_asistente_id_seq'::regclass);


--
-- TOC entry 2231 (class 2604 OID 178868)
-- Name: minuta_planteamiento id; Type: DEFAULT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_planteamiento ALTER COLUMN id SET DEFAULT nextval('minuta.minuta_planteamiento_id_seq'::regclass);


--
-- TOC entry 2263 (class 2604 OID 222772)
-- Name: reposicion_aux id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion_aux ALTER COLUMN id SET DEFAULT nextval('public.reposicion_aux_id_seq'::regclass);


--
-- TOC entry 2271 (class 2606 OID 178883)
-- Name: cargo cargo_descripcion_key; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.cargo
    ADD CONSTRAINT cargo_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 2273 (class 2606 OID 178885)
-- Name: cargo cargo_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY (id);


--
-- TOC entry 2275 (class 2606 OID 178887)
-- Name: ciudad ciudad_descripcion_key; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.ciudad
    ADD CONSTRAINT ciudad_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 2277 (class 2606 OID 178889)
-- Name: ciudad ciudad_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id);


--
-- TOC entry 2279 (class 2606 OID 178891)
-- Name: dependencia dependencia_descripcion_key; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.dependencia
    ADD CONSTRAINT dependencia_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 2281 (class 2606 OID 178893)
-- Name: dependencia dependencia_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.dependencia
    ADD CONSTRAINT dependencia_pkey PRIMARY KEY (id);


--
-- TOC entry 2283 (class 2606 OID 178895)
-- Name: ente ente_descripcion_key; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.ente
    ADD CONSTRAINT ente_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 2285 (class 2606 OID 178897)
-- Name: ente ente_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.ente
    ADD CONSTRAINT ente_pkey PRIMARY KEY (id);


--
-- TOC entry 2291 (class 2606 OID 178899)
-- Name: minuta_acuerdo minuta_acuerdo_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_acuerdo
    ADD CONSTRAINT minuta_acuerdo_pkey PRIMARY KEY (id);


--
-- TOC entry 2293 (class 2606 OID 178901)
-- Name: minuta_asistente minuta_asistente_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente
    ADD CONSTRAINT minuta_asistente_pkey PRIMARY KEY (id);


--
-- TOC entry 2287 (class 2606 OID 178903)
-- Name: minuta minuta_id_ciudad_fecha_hora_asunto_key; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta
    ADD CONSTRAINT minuta_id_ciudad_fecha_hora_asunto_key UNIQUE (id_ciudad, fecha, hora, asunto);


--
-- TOC entry 2289 (class 2606 OID 178905)
-- Name: minuta minuta_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta
    ADD CONSTRAINT minuta_pkey PRIMARY KEY (id);


--
-- TOC entry 2295 (class 2606 OID 178907)
-- Name: minuta_planteamiento minuta_planteamiento_pkey; Type: CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_planteamiento
    ADD CONSTRAINT minuta_planteamiento_pkey PRIMARY KEY (id);


--
-- TOC entry 2343 (class 2606 OID 222782)
-- Name: reposicion_aux reposicion_aux_id_reposicion_id_presentacion_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion_aux
    ADD CONSTRAINT reposicion_aux_id_reposicion_id_presentacion_key UNIQUE (id_reposicion, id_presentacion);


--
-- TOC entry 2345 (class 2606 OID 222780)
-- Name: reposicion_aux reposicion_aux_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion_aux
    ADD CONSTRAINT reposicion_aux_pkey PRIMARY KEY (id);


--
-- TOC entry 2339 (class 2606 OID 222760)
-- Name: reposicion reposicion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion
    ADD CONSTRAINT reposicion_pkey PRIMARY KEY (id);


--
-- TOC entry 2341 (class 2606 OID 222762)
-- Name: reposicion reposicion_reposicion_num_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion
    ADD CONSTRAINT reposicion_reposicion_num_key UNIQUE (reposicion_num);


--
-- TOC entry 2297 (class 2606 OID 178925)
-- Name: auditoria auditoria_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);


--
-- TOC entry 2329 (class 2606 OID 178927)
-- Name: usuario cedula_u; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT cedula_u UNIQUE (cedula);


--
-- TOC entry 2331 (class 2606 OID 178929)
-- Name: usuario correo_u; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT correo_u UNIQUE (correo);


--
-- TOC entry 2337 (class 2606 OID 206256)
-- Name: config_dblink dblink_connet_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.config_dblink
    ADD CONSTRAINT dblink_connet_pkey PRIMARY KEY (id);


--
-- TOC entry 2333 (class 2606 OID 178933)
-- Name: usuario id_usuario; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT id_usuario PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 178935)
-- Name: menu_0 menu_0_orden_key; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_0
    ADD CONSTRAINT menu_0_orden_key UNIQUE (orden);


--
-- TOC entry 2301 (class 2606 OID 178937)
-- Name: menu_0 menu_0_titulo_key; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_0
    ADD CONSTRAINT menu_0_titulo_key UNIQUE (titulo);


--
-- TOC entry 2303 (class 2606 OID 178939)
-- Name: menu_0 menu_abuelo_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_0
    ADD CONSTRAINT menu_abuelo_pkey PRIMARY KEY (id);


--
-- TOC entry 2307 (class 2606 OID 178941)
-- Name: menu_2 menu_hijo_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_2
    ADD CONSTRAINT menu_hijo_pkey PRIMARY KEY (id);


--
-- TOC entry 2309 (class 2606 OID 178943)
-- Name: menu_3 menu_nieto_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_3
    ADD CONSTRAINT menu_nieto_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 178945)
-- Name: menu_1 menu_padre_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_1
    ADD CONSTRAINT menu_padre_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 178947)
-- Name: msj msj_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.msj
    ADD CONSTRAINT msj_pkey PRIMARY KEY (id);


--
-- TOC entry 2313 (class 2606 OID 178949)
-- Name: pregunta_seguridad pregunta_pk; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.pregunta_seguridad
    ADD CONSTRAINT pregunta_pk PRIMARY KEY (id);


--
-- TOC entry 2315 (class 2606 OID 178951)
-- Name: rol_0 rol_0_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_0
    ADD CONSTRAINT rol_0_pkey PRIMARY KEY (id);


--
-- TOC entry 2317 (class 2606 OID 178953)
-- Name: rol_1 rol_1_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_1
    ADD CONSTRAINT rol_1_pkey PRIMARY KEY (id);


--
-- TOC entry 2319 (class 2606 OID 178955)
-- Name: rol_2 rol_2_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_2
    ADD CONSTRAINT rol_2_pkey PRIMARY KEY (id);


--
-- TOC entry 2321 (class 2606 OID 178957)
-- Name: rol_3 rol_3_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_3
    ADD CONSTRAINT rol_3_pkey PRIMARY KEY (id);


--
-- TOC entry 2323 (class 2606 OID 178959)
-- Name: roles roles_descripcion_key; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.roles
    ADD CONSTRAINT roles_descripcion_key UNIQUE (descripcion);


--
-- TOC entry 2325 (class 2606 OID 178961)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2327 (class 2606 OID 178963)
-- Name: status status_pkey; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- TOC entry 2335 (class 2606 OID 178965)
-- Name: usuario usuario_u; Type: CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT usuario_u UNIQUE (usuario);


--
-- TOC entry 2372 (class 2620 OID 222763)
-- Name: reposicion generar_numero; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER generar_numero BEFORE INSERT ON public.reposicion FOR EACH ROW EXECUTE PROCEDURE public.generar_numero();


--
-- TOC entry 2371 (class 2620 OID 178967)
-- Name: usuario auditoria_origen; Type: TRIGGER; Schema: seguridad; Owner: postgres
--

CREATE TRIGGER auditoria_origen AFTER INSERT OR DELETE OR UPDATE ON seguridad.usuario FOR EACH ROW EXECUTE PROCEDURE seguridad.auditoria_general();


--
-- TOC entry 2349 (class 2606 OID 178978)
-- Name: minuta_acuerdo minuta_acuerdo_id_minuta_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_acuerdo
    ADD CONSTRAINT minuta_acuerdo_id_minuta_fkey FOREIGN KEY (id_minuta) REFERENCES minuta.minuta(id);


--
-- TOC entry 2350 (class 2606 OID 178983)
-- Name: minuta_asistente minuta_asistente_id_cargo_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente
    ADD CONSTRAINT minuta_asistente_id_cargo_fkey FOREIGN KEY (id_cargo) REFERENCES minuta.cargo(id);


--
-- TOC entry 2351 (class 2606 OID 178988)
-- Name: minuta_asistente minuta_asistente_id_dependencia_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente
    ADD CONSTRAINT minuta_asistente_id_dependencia_fkey FOREIGN KEY (id_dependencia) REFERENCES minuta.dependencia(id);


--
-- TOC entry 2352 (class 2606 OID 178993)
-- Name: minuta_asistente minuta_asistente_id_ente_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente
    ADD CONSTRAINT minuta_asistente_id_ente_fkey FOREIGN KEY (id_ente) REFERENCES minuta.ente(id);


--
-- TOC entry 2353 (class 2606 OID 178998)
-- Name: minuta_asistente minuta_asistente_id_minuta_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_asistente
    ADD CONSTRAINT minuta_asistente_id_minuta_fkey FOREIGN KEY (id_minuta) REFERENCES minuta.minuta(id);


--
-- TOC entry 2346 (class 2606 OID 179003)
-- Name: minuta minuta_id_ciudad_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta
    ADD CONSTRAINT minuta_id_ciudad_fkey FOREIGN KEY (id_ciudad) REFERENCES minuta.ciudad(id);


--
-- TOC entry 2347 (class 2606 OID 179008)
-- Name: minuta minuta_id_dependencia_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta
    ADD CONSTRAINT minuta_id_dependencia_fkey FOREIGN KEY (id_dependencia) REFERENCES minuta.dependencia(id);


--
-- TOC entry 2348 (class 2606 OID 179013)
-- Name: minuta minuta_id_ente_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta
    ADD CONSTRAINT minuta_id_ente_fkey FOREIGN KEY (id_ente) REFERENCES minuta.ente(id);


--
-- TOC entry 2354 (class 2606 OID 179018)
-- Name: minuta_planteamiento minuta_planteamiento_id_minuta_fkey; Type: FK CONSTRAINT; Schema: minuta; Owner: postgres
--

ALTER TABLE ONLY minuta.minuta_planteamiento
    ADD CONSTRAINT minuta_planteamiento_id_minuta_fkey FOREIGN KEY (id_minuta) REFERENCES minuta.minuta(id);


--
-- TOC entry 2370 (class 2606 OID 222783)
-- Name: reposicion_aux reposicion_aux_id_reposicion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reposicion_aux
    ADD CONSTRAINT reposicion_aux_id_reposicion_fkey FOREIGN KEY (id_reposicion) REFERENCES public.reposicion(id);


--
-- TOC entry 2368 (class 2606 OID 179028)
-- Name: usuario id_status_fk; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT id_status_fk FOREIGN KEY (id_status) REFERENCES seguridad.status(id) ON UPDATE CASCADE;


--
-- TOC entry 2355 (class 2606 OID 179033)
-- Name: menu_2 menu_2_id_status_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_2
    ADD CONSTRAINT menu_2_id_status_fkey FOREIGN KEY (id_status) REFERENCES seguridad.status(id);


--
-- TOC entry 2356 (class 2606 OID 179038)
-- Name: menu_3 menu_3_id_status_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.menu_3
    ADD CONSTRAINT menu_3_id_status_fkey FOREIGN KEY (id_status) REFERENCES seguridad.status(id);


--
-- TOC entry 2357 (class 2606 OID 179043)
-- Name: rol_0 rol_0_id_menu_0_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_0
    ADD CONSTRAINT rol_0_id_menu_0_fkey FOREIGN KEY (id_menu_0) REFERENCES seguridad.menu_0(id);


--
-- TOC entry 2358 (class 2606 OID 179048)
-- Name: rol_0 rol_0_id_rol_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_0
    ADD CONSTRAINT rol_0_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES seguridad.roles(id);


--
-- TOC entry 2359 (class 2606 OID 179053)
-- Name: rol_1 rol_1_id_menu_0_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_1
    ADD CONSTRAINT rol_1_id_menu_0_fkey FOREIGN KEY (id_menu_0) REFERENCES seguridad.menu_0(id);


--
-- TOC entry 2360 (class 2606 OID 179058)
-- Name: rol_1 rol_1_id_menu_1_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_1
    ADD CONSTRAINT rol_1_id_menu_1_fkey FOREIGN KEY (id_menu_1) REFERENCES seguridad.menu_1(id);


--
-- TOC entry 2361 (class 2606 OID 179063)
-- Name: rol_1 rol_1_id_rol_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_1
    ADD CONSTRAINT rol_1_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES seguridad.roles(id);


--
-- TOC entry 2362 (class 2606 OID 179068)
-- Name: rol_2 rol_2_id_menu_1_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_2
    ADD CONSTRAINT rol_2_id_menu_1_fkey FOREIGN KEY (id_menu_1) REFERENCES seguridad.menu_1(id);


--
-- TOC entry 2363 (class 2606 OID 179073)
-- Name: rol_2 rol_2_id_menu_2_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_2
    ADD CONSTRAINT rol_2_id_menu_2_fkey FOREIGN KEY (id_menu_2) REFERENCES seguridad.menu_2(id);


--
-- TOC entry 2364 (class 2606 OID 179078)
-- Name: rol_2 rol_2_id_rol_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_2
    ADD CONSTRAINT rol_2_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES seguridad.roles(id);


--
-- TOC entry 2365 (class 2606 OID 179083)
-- Name: rol_3 rol_3_id_menu_2_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_3
    ADD CONSTRAINT rol_3_id_menu_2_fkey FOREIGN KEY (id_menu_2) REFERENCES seguridad.menu_2(id);


--
-- TOC entry 2366 (class 2606 OID 179088)
-- Name: rol_3 rol_3_id_menu_3_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_3
    ADD CONSTRAINT rol_3_id_menu_3_fkey FOREIGN KEY (id_menu_3) REFERENCES seguridad.menu_3(id);


--
-- TOC entry 2367 (class 2606 OID 179093)
-- Name: rol_3 rol_3_id_rol_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.rol_3
    ADD CONSTRAINT rol_3_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES seguridad.roles(id);


--
-- TOC entry 2369 (class 2606 OID 179098)
-- Name: usuario usuario_id_rol_fkey; Type: FK CONSTRAINT; Schema: seguridad; Owner: postgres
--

ALTER TABLE ONLY seguridad.usuario
    ADD CONSTRAINT usuario_id_rol_fkey FOREIGN KEY (id_rol) REFERENCES seguridad.roles(id);


-- Completed on 2020-07-11 22:22:35 -04

--
-- PostgreSQL database dump complete
--

