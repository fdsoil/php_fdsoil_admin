<?php
use \FDSoil\DbFunc;
use \myApp0\Usuario;

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function valPswdOld() { echo Usuario::valPswdOld(); }

    private function usuarioGet() { echo json_encode(DbFunc::fetchAllAssoc(Usuario::usuarioList())); }

    private function buscarXCed() { echo json_encode(DbFunc::fetchAllAssoc(Usuario::buscarXCed())); }

    private function validarCedulaId() { echo Usuario::validarCedulaId(); }

    private function validarEmail() { echo Usuario::validarEmail(); }

    private function validarCelular() { echo Usuario::validarCelular(); }

    private function preguntaSecreta() { echo Usuario::preguntaSecreta(); }

    private function validarRespSeguridad() { echo Usuario::validarRespSeguridad(); }

    private function comprobarDisponibilidad() { echo Usuario::comprobarDisponibilidad($_POST); }

}

