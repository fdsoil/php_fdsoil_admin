<?php
use \FDSoil\AutoCrud as AutoCrud;

class SubIndex
{
    private $_obj;

    public function __construct($method)
    { 
        $this->_obj = new AutoCrud($_POST['bd']);
        self::$method();
    }

    private function getTablesOfBd() { echo $this->_obj->getTablesOfBd(); }

}

