<?php

class Seniat
{

    /**
     * @propiedad: De Nadie
     * @Autor: Gregorio Bolivar
     * @email: elalconxvii@gmail.com
     * @Fecha de Creacion: 05/09/2015
     * @Auditado por: Gregorio J Bolivar B
     * @Fecha de Modificacion: 12/02/2016
     * @Descripcin: Encargado de Buscar la Razón Social ante el Seniat
     * @package: Seniat.class.php
     * @version: 2.0
    */

    public function razonSocial($rif)
    {
        if (preg_match("/^[JGVEPC][0-9]{9}$/",$rif)){
            $url = curl_init("http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=$rif"); 
            curl_setopt ($url, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($url, CURLOPT_CUSTOMREQUEST, 'GET');//esto si usa metodo GET
            //curl_setopt ($ch, CURLOPT_POST, 1);//esto si usa metodo POST
            //curl_setopt ($ch, CURLOPT_POSTFIELDS, $cadena_busqueda );
            /* Comentar las dos siguientes líneas si su plataforma no usa proxy */
            //curl_setopt ($url, CURLOPT_PROXY, "http://192.168.0.5");
            //curl_setopt ($url, CURLOPT_PROXYPORT, 8080);
            $resultado = curl_exec ($url);
            if ($resultado) {
                try {
                    if (substr($resultado, 0, 1) != '<')
                        throw new Exception($resultado);
                    $xml = simplexml_load_string($resultado);
                    if (is_object($xml)) {
                        $elements = $xml->children('rif');
                        $seniat = array();
                        $response_json['result'] = 1;
                        foreach ($elements as $indice => $node) {
                            $index = strtolower($node->getName());
                            $seniat[$index] = (string) $node;
                        }
                        $response_json['data'] = $seniat;
                    }
                    else{
                        $response_json['result'] = 0;
                        $response_json['data'] = 'Falla en la búsqueda...';               
                    }
    
                } catch (Exception $e) { 
                    $result = explode(' ', utf8_encode(@$resultado), 2);
                    $response_json['result'] = (int) $result[0];
                    $response_json['data'] = (string) $result[1];
                }
            } else {
                $response_json['result'] = 0;
                $response_json['data'] = 'Error de conexión...';
            }
        } else {
            $response_json['result'] = 0;
            $response_json['data'] = 'Formato inválido...';     
        }
        return json_encode($response_json);
    }
}

die(Seniat::razonSocial($_GET['rif']));

