<?php
use \FDSoil\Func;
use \FDSoil\DbFunc;
use \myApp0\Usuario\Rol\Menu;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);  
        $aId['id']=$_POST['id_n1'];
        $rowOpt_1=DbFunc::fetchRow(Menu::menuNivel_1_idTitulo($aId));
        $xtpl->assign('DES_OPCION_0', $rowOpt_1[0]);
        $xtpl->assign('ID_OPCION_1', $rowOpt_1[1]);
        $xtpl->assign('DES_OPCION_1', $rowOpt_1[2]);
        if (!empty($_POST)){
            $aId['id']=$_POST['id'];
            $rowOpt=DbFunc::fetchRow(Menu::menuNivel_2_RowEdit($aId));
            $xtpl->assign('ID_ROW', $rowOpt[0]);
            $xtpl->assign('ORDEN', $rowOpt[1]);
            $xtpl->assign('OPCION', $rowOpt[2]);
            $xtpl->assign('RUTA', $rowOpt[3]);
        }
        $result = Menu::menuStatusList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            if (!empty($_POST)) 
                $xtpl->assign('SELECTED_STATUS', ($rowOpt[4] == $row[1]) ? 'selected' : '');
            $xtpl->parse('main.status');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return"], ["btnName"=>"Save"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

