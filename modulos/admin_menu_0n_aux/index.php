<?php
use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;
use \myApp0\Usuario\Rol\Menu as Menu;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        if (!empty($_POST)){
            $rowOpt = DbFunc::fetchRow(Menu::menuNivel_0_rowEdit());
            $xtpl->assign('ID_ROW', $rowOpt[0]);
            $xtpl->assign('ORDEN', $rowOpt[1]);
            $xtpl->assign('OPCION', $rowOpt[2]);
            $xtpl->assign('RUTA', $rowOpt[3]);
        }
        $result = Menu::menuStatusList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            if (!empty($_POST)) 
                $xtpl->assign('SELECTED_STATUS', ($rowOpt[4] == $row[1]) ? 'selected' : '');
            $xtpl->parse('main.status');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return"], ["btnName"=>"Save"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

