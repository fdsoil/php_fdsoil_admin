<?php
use \FDSoil\Func;
use \myApp0\Usuario\Rol;
use \myApp0\Usuario\Rol\Menu;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);  
        $xtpl->assign('ID', (isset($_POST['id']))?$_POST['id']:0);
        $rowRol = \myApp0\Usuario\Rol::rolRow();   
        $xtpl->assign('ROL_NAME', $rowRol[0]);  
        $xtpl->assign('ROL_PAG_INI_DEFAULT', $rowRol[2]);  
        $aView['load'] = "[" . $_SESSION['menu']. "," . Menu::menuOptMake() . "," . Rol::rolOpt(). "]";
        $result = Menu::menuStatusList(); 
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            if (!empty($_POST)) 
                $xtpl->assign('SELECTED_STATUS', ($rowRol[1] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.status');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return"], ["btnName"=>"Save", "btnClick"=>"sweepTable();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

