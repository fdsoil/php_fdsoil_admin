function optMenuHave(arrMenu)
{

    this.optHasDependent = function (idRepresentante,arrDependiente)
    {
	var response=false;
	for (var j=0; j < arrDependiente.length; j++){
			var dependienteRow = arrDependiente[j];
			if (idRepresentante==dependienteRow[1]){
				response=true;
				break;
			}
		}
	return 	response;
    }

    this.row0Add = function row0Add(id,opt,flag)
    {

        var tr  = document.createElement('tr');

        var td = document.createElement('td');	
        var node = document.createTextNode(id);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);

        td = document.createElement('td');
        node = document.createTextNode(opt);
        colspan = (flag=='o')?'4':'5';	
        td.setAttribute('colspan',colspan);
        if (flag=='t') 
	    td.setAttribute('style','font-weight:bold');
        td.appendChild(node);
        tr.appendChild(td);    
  
        if (flag=='o'){
            td=document.createElement('td');
            td.setAttribute('style','width:10%;');
            var input_chk=document.createElement('input');
            input_chk.setAttribute('type','checkbox');
            input_chk.setAttribute('title','Seleccione el Producto...');
            td.appendChild(input_chk);
            tr.appendChild(td);
        }   

        td = document.createElement('td');	
        td.setAttribute('style','color:#FFFFFF');
        node = document.createTextNode(flag);        
        td.appendChild(node);
        tr.appendChild(td);   
    
        document.getElementById('tbl').appendChild(tr);
    
    }

    this.row1Add = function row1Add(id0,id,opt,flag)
    {

        var tr  = document.createElement('tr');
    
        var td = document.createElement('td');	
        var node = document.createTextNode(id0);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);

        td = document.createElement('td');
        node = document.createTextNode(id);
        td.setAttribute('style','color:#FFFFFF');	
        td.appendChild(node);
        tr.appendChild(td);  
    
        td = document.createElement('td');	
        node = document.createTextNode(opt);
        colspan = (flag=='o')?'3':'4';
        td.setAttribute('colspan',colspan);
        if (flag=='t') 
	    td.setAttribute('style','font-weight:bold');
        td.appendChild(node);
        tr.appendChild(td); 

        if (flag=='o'){  
	    td=document.createElement('td');
    	    td.setAttribute('style','width:10%;');
    	    var input_chk=document.createElement('input');
    	    input_chk.setAttribute('type','checkbox');
    	    input_chk.setAttribute('title','Seleccione el Producto...');
    	    td.appendChild(input_chk);
    	    tr.appendChild(td);  
        }

        var td = document.createElement('td');
        td.setAttribute('style','color:#FFFFFF');
        var node = document.createTextNode(flag);
        td.appendChild(node);
        tr.appendChild(td);  
    
        document.getElementById('tbl').appendChild(tr);
    
    }

    this.row2Add = function row2Add(id0,id1,id,opt,flag)
    {

        var tr  = document.createElement('tr');
    
        var td = document.createElement('td');	
        var node = document.createTextNode(id0);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);

        td = document.createElement('td');	
        node = document.createTextNode(id1);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);
    
        td = document.createElement('td');	
        node = document.createTextNode(id);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td); 
    
        td = document.createElement('td');	
        node = document.createTextNode(opt);
        colspan = (flag=='o')?'2':'3';
        td.setAttribute('colspan',colspan);
        if (flag=='t') 
            td.setAttribute('style','font-weight:bold');
        td.appendChild(node);
        tr.appendChild(td);    

        if (flag=='o'){      
	    td=document.createElement('td');
    	    td.setAttribute('style','width:10%;');
    	    var input_chk=document.createElement('input');
    	    input_chk.setAttribute('type','checkbox');
    	    input_chk.setAttribute('title','Seleccione el Producto...');
    	    td.appendChild(input_chk);
    	    tr.appendChild(td); 
        }
  
        td = document.createElement('td');
        td.setAttribute('style','color:#FFFFFF');
        node = document.createTextNode(flag);
        td.appendChild(node);
        tr.appendChild(td); 
    
        document.getElementById('tbl').appendChild(tr);
    
    }

    this.row3Add = function row3Add(id0,id1,id2,id,opt,flag)
    {

        var tr  = document.createElement('tr');

        var td = document.createElement('td');	
        var node = document.createTextNode(id0);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);
    
        td = document.createElement('td');	
        node = document.createTextNode(id1);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);
    
        td = document.createElement('td');	
        node = document.createTextNode(id2);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);
    
        td = document.createElement('td');	
        node = document.createTextNode(id);
        td.setAttribute('style','color:#FFFFFF');
        td.appendChild(node);
        tr.appendChild(td);    
    
        td = document.createElement('td');	
        node = document.createTextNode(opt);
        td.appendChild(node);
        tr.appendChild(td);    
    
        td=document.createElement('td');
        td.setAttribute('style','width:10%;');
        input_chk=document.createElement('input');
        input_chk.setAttribute('type','checkbox');
        input_chk.setAttribute('title','Seleccione el Producto...');
        td.appendChild(input_chk);
        tr.appendChild(td);

        td = document.createElement('td');
        td.setAttribute('style','color:#FFFFFF');
        node = document.createTextNode(flag);
        td.appendChild(node);
        tr.appendChild(td);    
    
        document.getElementById('tbl').appendChild(tr);
    
    }

    for (a=0; a < arrMenu[0].length; a++){
	var row0 = arrMenu[0][a];
	var id0=row0[0];
	var hasRow1=false;
	hasRow1=optHasDependent(id0,arrMenu[1]);
	if (hasRow1==false)
            row0Add(row0[0],row0[1],'o');
	else{
            row0Add(row0[0],row0[1],'t');
            for (p=0; p < arrMenu[1].length; p++){
                var row1 = arrMenu[1][p];
		if (id0==row1[1]){
                    var id1=row1[0];
                    var hasRow2=false;					
                    hasRow2=optHasDependent(id1,arrMenu[2]);
                    if (hasRow2==false)
			row1Add(row1[1],row1[0],row1[2],'o');                    
                    else{
                        row1Add(row1[1],row1[0],row1[2],'t');
        		for (h=0; h < arrMenu[2].length; h++){
                            var row2 = arrMenu[2][h];
                            if (id1==row2[1]){
				var id2=row2[0];
                        	var hasRow3=false;
				hasRow3=optHasDependent(id2,arrMenu[3]);
				if (hasRow3==false)
                                    row2Add(row1[1],row2[1],row2[0],row2[2],'o');				
				else{
                                    row2Add(row1[1],row2[1],row2[0],row2[2],'t');
                                    for (n=0; n < arrMenu[3].length; n++){
					var row3 = arrMenu[3][n];
					if (id2==row3[1])
                                            row3Add(row1[1],row2[1],row3[1],row3[0],row3[2],'o');                                        
                                    }
				}									
                            }
			}
                    }
                }
            }
        }
    }						
}

function putOptRol(arrRolOpt)
{

    this.putOptRolAuxNivel0 = function (value)
    {
        var oTbl=document.getElementById('tbl');
        var oTrs=oTbl.getElementsByTagName('tr');
        for(var i=1;i<oTrs.length;i++){
            var oTds=oTrs[i].getElementsByTagName('td');
            if (oTds.length==4 && oTds[3].innerHTML=='o' && value==oTds[0].innerHTML)
                oTds[2].firstChild.checked=true;
        }
    }

    this.putOptRolAuxNivel1 = function (arreglo)
    {
        var oTbl=document.getElementById('tbl');
        var oTrs=oTbl.getElementsByTagName('tr');
        for(var i=1;i<oTrs.length;i++){
            var oTds=oTrs[i].getElementsByTagName('td');
            if (oTds.length==5 && oTds[4].innerHTML=='o' && arreglo[0]==oTds[0].innerHTML && arreglo[1]==oTds[1].innerHTML )
                oTds[3].firstChild.checked=true;
        }
    }

    this.putOptRolAuxNivel2 = function (arreglo)
    {
        var oTbl=document.getElementById('tbl');
        var oTrs=oTbl.getElementsByTagName('tr');
        for(var i=1;i<oTrs.length;i++){
            var oTds=oTrs[i].getElementsByTagName('td');
            if (oTds.length==6 && oTds[5].innerHTML=='o' && arreglo[0]==oTds[0].innerHTML && 
            arreglo[1]==oTds[1].innerHTML && arreglo[2]==oTds[2].innerHTML)
                oTds[4].firstChild.checked=true;
        }
    }

    this.putOptRolAuxNivel3 = function (arreglo)
    {
        var oTbl=document.getElementById('tbl');
        var oTrs=oTbl.getElementsByTagName('tr');
        for(var i=1;i<oTrs.length;i++){
            var oTds=oTrs[i].getElementsByTagName('td');
            if (oTds.length==7 && oTds[6].innerHTML=='o' && arreglo[0]==oTds[0].innerHTML && 
            arreglo[1]==oTds[1].innerHTML && arreglo[2]==oTds[2].innerHTML && arreglo[3]==oTds[3].innerHTML)
                oTds[5].firstChild.checked=true;
        }
    }

    for(var i in arrRolOpt[0])
        putOptRolAuxNivel0(arrRolOpt[0][i]);
    for(var i in arrRolOpt[1])
        putOptRolAuxNivel1(arrRolOpt[1][i]);
    for(var i in arrRolOpt[2])
        putOptRolAuxNivel2(arrRolOpt[2][i]);
    for(var i in arrRolOpt[3])
        putOptRolAuxNivel3(arrRolOpt[3][i]);

}

function sweepTable()
{

    this.isThereThisValueInTheArray = function (arreglo, valor) 
    {
       var resp = false;
       for(var i in arreglo)
          if (arreglo[i] == valor) 
             resp = true
       return resp;
    }

    var oTbl=document.getElementById('tbl');
    var oTrs=oTbl.getElementsByTagName('tr');
    var arreglo0=new Array();
    var arreglo1=new Array();
    var arreglo2=new Array();
    var arreglo3=new Array();
    var a=0;
    var b=0;
    var c=0;
    var d=0;
    for(var i=1;i<oTrs.length;i++){
        var oTds=oTrs[i].getElementsByTagName('td');
        var id0='';
        var id1='';
        var id2=''; 
        var id3='';               
        if (oTds.length==4 && oTds[3].innerHTML=='o' && oTds[2].firstChild.checked){
           id0=oTds[0].innerHTML;
           if (!isThereThisValueInTheArray(arreglo0,id0))
              arreglo0[a++]=id0;
        }else if (oTds.length==5 && oTds[4].innerHTML=='o' && oTds[3].firstChild.checked){
            id0=oTds[0].innerHTML;
            if (!isThereThisValueInTheArray(arreglo0,id0))
	        arreglo0[a++]=id0;
	    id1=oTds[1].innerHTML;
            if (!isThereThisValueInTheArray(arreglo1,id0+'|'+id1))
                arreglo1[b++]=id0+'|'+id1;
        }else if (oTds.length==6 && oTds[5].innerHTML=='o' && oTds[4].firstChild.checked){
            id0=oTds[0].innerHTML;
            if (!isThereThisValueInTheArray(arreglo0,id0))
                arreglo0[a++]=id0;
            id1=oTds[1].innerHTML;
            if (!isThereThisValueInTheArray(arreglo1,id0+'|'+id1))
                arreglo1[b++]=id0+'|'+id1;
            id2=oTds[2].innerHTML; 
            if (!isThereThisValueInTheArray(arreglo2,id1+'|'+id2))
                arreglo2[c++]=id1+'|'+id2;
        }else if (oTds.length==7 && oTds[6].innerHTML=='o' && oTds[5].firstChild.checked){
            id0=oTds[0].innerHTML;
            if (!isThereThisValueInTheArray(arreglo0,id0))
                arreglo0[a++]=id0;
	    id1=oTds[1].innerHTML;
            if (!isThereThisValueInTheArray(arreglo1,id0+'|'+id1))
                arreglo1[b++]=id0+'|'+id1;
	    id2=oTds[2].innerHTML; 
            if (!isThereThisValueInTheArray(arreglo2,id1+'|'+id2))
                arreglo2[c++]=id1+'|'+id2;
            id3=oTds[3].innerHTML;
            if (!isThereThisValueInTheArray(arreglo3,id2+'|'+id3))
                arreglo3[d++]=id2+'|'+id3;
        }	
    }
    document.getElementById('id_str_array_n0').value=arreglo0.join('¬');
    document.getElementById('id_str_array_n1').value=arreglo1.join('¬');
    document.getElementById('id_str_array_n2').value=arreglo2.join('¬');
    document.getElementById('id_str_array_n3').value=arreglo3.join('¬');
    document.forms[0].submit();
}
