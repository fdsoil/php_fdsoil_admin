<?php
use \FDSoil\Func;

class SubIndex
{
    public function execute()
    {
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = "";
        $aView['load'] = [];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

