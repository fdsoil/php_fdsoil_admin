function sendComboNivel_1(id)
{
    var responseAjax = (response) =>
    {
        llenarCombo(document.getElementById('id_nivel_1'),response.split('¬'),'|','null','Seleccione...');
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + aMyApp[0] + "/reqs/control/menu_rol/index.php/menuNivel1Combo";
    ajax.data = "id_nivel_0=" + id;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();    	
}

function sendComboNivel_2(id)
{
    var responseAjax = (response) =>
    {
        llenarCombo(document.getElementById('id_nivel_2'),response.split('¬'),'|','null','Seleccione...');    
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + aMyApp[0] + "/reqs/control/menu_rol/index.php/menuNivel2Combo";
    ajax.data = "id_nivel_1=" + id;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();	
}

function sendTablaNivel_3(id)
{
    var responseAjax = (response) =>
    {
        (response)?llenarTabla(response.split('¬'),'|'):llenarTabla('','|'); 
        jQryTableRefresh('tablaRows');
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + aMyApp[0] + "/reqs/control/menu_rol/index.php/menuNivel3Tabla";
    ajax.data = "id_nivel_2=" + id;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send(); 	
}

