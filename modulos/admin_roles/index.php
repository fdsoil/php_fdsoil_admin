<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Perfil"]);
        $result = \myApp0\Usuario\Rol::rolList();
        while ($row = \FDSoil\DbFunc::fetchRow($result)){
            $xtpl->assign('DESCRIPCION', $row[1]);
            $xtpl->assign('PAG_INI', $row[2]);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row[0]]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row[0]]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

