<?php
use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

class SubIndex
{
    public function execute($aReqs)
    {
        \FDSoil\Audit::validaReferenc();
        $oJSON = json_decode(file_get_contents(__DIR__."/js/include.json"));
        $oJSON->path = $aReqs['path'] . '/';
        $oJSON = json_encode($oJSON);
        $aView['include'] = json_encode($oJSON);
        $aView['userData'] = '';
        $aView['load'] = [];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $msj = '';//GHIJKLMNOPQ
        $get['msj']=$aReqs['params'][0];
        $get['np']=$aReqs['params'][1];
        if ($get['msj'] == 'A') //1
            $msj = 'El registro fue actualizado con éxito.';
        else if ($get['msj'] == 'B') //2
            $msj = 'El registro fue eliminado con éxito.';
        else if ($get['msj'] == 'C') //3
            $msj = 'El registro fue agregado con éxito.';
        else if ($get['msj'] == 'D') //4
            $msj = 'La contraseña fue restablecida con éxito; será enviada a su correo.';
        else if ($get['msj'] == 'E') //6
            $msj = 'Contraseña reseteada con éxito, será enviada al correo del usuario.'; 
        else if ($get['msj'] == 'F') //7
            $msj = 'Su clave fue cambiada con éxito, debe volver a ingresar al sistema.';
        else if ($get['msj'] == 'G') //8
            $msj = 'El registro fue reversado con éxito.';
        else if ($get['msj'] == 'M') //
	    $msj = 'Disculpe: Hay Registro(s) Bloqueado(s)...';
        else if ($get['msj'] == 'N') //
	    $msj = 'Validando el Formulario Enviado... ';
        else if ($get['msj'] == 'O') //93
	    $msj = 'Disculpe: La clave secreta no es valida.';
        else if ($get['msj'] == 'P') //94
	    $msj = 'Debe llenar el siguiente registro antes de realizar esta acción.';
        else if ($get['msj'] == 'Q') //95
	    $msj = 'Debe completar el siguiente registro antes de realizar esta acción.';
        else if ($get['msj'] == 'R') //96
            $msj = 'Error: El id suministrado no existe.';
        else if ($get['msj'] == 'S') //85
            $msj = 'Disculpe: El correo electrónico introducido ya está registrado por otro usuario.';
        else if ($get['msj'] == 'T') //98
            $msj = 'Disculpe: El registro está repetido.';
        else if ($get['msj'] == 'U') //94-97
            $msj = 'Disculpe: Hay registro(s) asociado(s).';
        else if ($get['msj'] == 'V') //93
            $msj = 'Error: Respuesta secreta incorrecta.';
        else if ($get['msj'] == 'W') //91
            $msj = 'Error: Correo no enviado; notifique al especialista del sistema.';
        else if ($get['msj'] == 'X') //90
            $msj = 'Disculpe: No tiene información asociada.';
        else if ($get['msj'] == 'Y') //86
            $msj = 'Disculpe: Por razones de seguridad debe cambiar su clave.';
        else if ($get['msj'] == 'Z') //99
            $msj = 'Error de ejecución; notifique al especialista del sistema.';
        else{
            $aId['id'] = $get['msj'];
            $row = DbFunc::fetchRow( DbFunc::getMsj($aId) );
            $msj = ($row[0]!=null)?$row[0]:'Error de ejecución desconocido.';
        }
        (Func::isThereThisKeyInTheArray($_SESSION,'messages'))?self::msgList($msj,$xtpl):self::msgOnly($msj,$xtpl);
        if (Func::isThereThisKeyInTheArray($_SESSION, 'dp')){
            $ndp = $_SESSION['dp'];
            unset($_SESSION['dp']);
        }
        else
            $ndp = $get['np'];
        $xtpl->assign('NUM_PAG', $ndp);
        $xtpl->assign('PATH', $aReqs['path']);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }

    private function msgOnly($msj, $xtpl)
    {
        $xtpl->assign('CLASS_TR','losnone');
        $xtpl->assign('TITLE', 'Mensaje(s)');
        $xtpl->assign('MSG', $msj);
        $xtpl->parse('main.row');
    }

    private function msgList($titulo, $xtpl)
    {
        $xtpl->assign('TITLE', $titulo);
        $array = $_SESSION['messages'];
        $classTR='losnone';
        for ($i = 0; $i < count($array); $i++) {
            $xtpl->assign('CLASS_TR',$classTR);
            $xtpl->assign('MSG', $array[$i]);
            $xtpl->parse('main.row');
            $classTR=($classTR=='losnone')?'lospare':'losnone';
        }
        unset($_SESSION['messages']);
    }
}

