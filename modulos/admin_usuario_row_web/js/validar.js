function val_envio(){
    if ((validateObjs(document.getElementById('id_panel_registrar_usuario')))&&(document.getElementById('id_controlPasswd').value!=0)) {
        document.forms[0].action='../admin_usuario_row_web/save/';
        document.forms[0].submit();
    }  
}

function val_clave(id_destino){
    var obj=new appPasswrd();
    clave=document.getElementById('id_clave');
    clave2=document.getElementById('id_clave2');
    if(clave.value !='' && clave2.value!=''){
        if (clave.value != clave2.value ){
            document.getElementById(id_destino).innerHTML='Las claves no coinciden.';
        }else{
            document.getElementById(id_destino).innerHTML='';
        }
    }else{
        document.getElementById(id_destino).innerHTML='';
    }
    if (validatePassWord(document.getElementById('id_clave').value)==false){//,id_destino
        document.getElementById('id_controlPasswd').value=0;
        document.getElementById('div_clave').innerHTML='Debe estar conformada por letras mayúsculas y minúsculas, números, caracteres especiales y el largo debe ser de '+obj.rangeMin+' a '+obj.rangeMax+' dígitos';   
    }else{
        document.getElementById('id_controlPasswd').value=1;
        document.getElementById('div_clave').innerHTML='';
    }   
}

document.getElementById('celular_cod').addEventListener("click", function() {
    pickOpen('celular_cod', 'id_celular_cod', '../../../../../' + appOrg + '/pickList/control/tel_cod_area_cel/', 15, 25, 400, 100, 'si');
});


document.getElementById('local_cod').addEventListener("click", function() {
    pickOpen('local_cod', 'id_local_cod', '../../../' + appOrg + '/pickList/control/tel_cod_area_nac/', 15, 25, 400, 100, 'si'); 
});

document.getElementById('local_cod2').addEventListener("click", function() {
    pickOpen('local_cod2', 'id_local_cod2', '../../../../../' + appOrg + '/pickList/control/tel_cod_area_nac/', 15, 25, 400, 100, 'si');
});

document.getElementById('id_nacionalidad').addEventListener("change", function() {
    sendCedula(this.value, document.getElementById("id_cedula").value);
});

document.getElementById('id_cedula').addEventListener("blur", function() {
    sendCedula(document.getElementById("id_nacionalidad").value, this.value);
});

document.getElementById('id_usuario').addEventListener("keyup", function() {
    sendUsuario(this.value);
});

document.getElementById('id_clave').addEventListener("blur", function() {
    val_clave('div_clave2');
});

document.getElementById('id_clave').addEventListener("focus", function() {
    val_clave('div_clave2');
});

document.getElementById('id_clave2').addEventListener("blur", function() {
    val_clave('div_clave2');
});

document.getElementById('id_clave2').addEventListener("focus", function() {
    val_clave('div_clave2');
});

document.getElementById('id_salir').addEventListener("click", function() {
    location.href='../admin_acceso/';
});

document.getElementById('id_Guardar').addEventListener("click", function() {
    val_envio();
});




