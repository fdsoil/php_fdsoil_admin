<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);     
        $result = \FDSoil\AutoCrud::getSchemasOfBd();
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
            $xtpl->assign('ID', $row[0]);
            $xtpl->assign('DESCRIPCION',  $row[1]);            
            $xtpl->parse('main.combo_multiple');
        }   
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

