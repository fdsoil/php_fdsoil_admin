<?php
use \FDSoil\Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Menu (Nivel 1)", 
                                    "btnId"=>"document.getElementById('id_nivel_0').value",
                                    "btnDisplay"=>"none"]);
        $result = \myApp0\Usuario\Rol\Menu::menuNivel_0_combo();
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_MENU_0', $row[0]);
            $xtpl->assign('DES_MENU_0', $row[1]);	
            $xtpl->parse('main.combo_0');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

