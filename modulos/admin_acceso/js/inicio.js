function inicio()
{  
    if (validateTheBrowser()){//checkCookie();
        initView();	
        var objAcceso=new appAcceso();
        id_acceso(objAcceso.email,objAcceso.telefon,objAcceso.webUserRow);
        loginType(objAcceso.loginType);   
        loginOptOnOff();
        webUserRow(objAcceso.webUserRow);
	onOffCaptcha(objAcceso.captcha);	
	keypad(objAcceso.keypad);
        if (objAcceso.popUp.show)
            imgInfo('cuerpo', objAcceso.popUp.info, objAcceso.popUp.timeSleep, objAcceso.popUp.width, objAcceso.popUp.height);
    }else
        window.location = "../../../"+FDSoil.toLowerCase()+"/admin_update_the_browser/";    
}

function onOffCaptcha(bValue)
{
	if (bValue){		
		document.getElementById('id_div_captcha').style.display='block';
		drawCaptcha();
	}
	else
		document.getElementById('id_div_captcha').style.display='none';		
}

function loginType(vector)
{
    document.getElementById('id_opcion').value = vector[0];
    document.getElementById('id_login_type').style.display = vector[1];
}

function acceso_route()
{
    var obj=new app();
    window.location.href=obj.strRoute;
}

function webUserRow(vector)
{
    document.getElementById('id_register').style.display = inArray( vector[0], ['','none'] ) ? vector[0] : '';
    document.getElementById('id_recover_password').style.display = inArray( vector[1], ['','none'] ) ? vector[1] : '';
}

function validarForm() 
{
    var respond = false;
    var idCombo=document.getElementById('id_opcion');    
    var idRif=document.getElementById('id_rif2');
    var idCedula=document.getElementById('id_cedula');
    var idEmail=document.getElementById('id_correo');
    var idUsuario=document.getElementById('id_user');    
    var idError = document.getElementById('id_msj_error');
    var idClave = document.getElementById('id_clave');  
    var idCaptcha = document.getElementById('txtInput'); 
    var idErrorCaptcha = document.getElementById('id_msj_error_captcha');
    var idDivCaptcha = document.getElementById('id_div_captcha');
    var bCaptcha=idDivCaptcha.style.display=='block'?true:false;
    var respCaptcha=true;
    if (idCombo.value==1)        
        if (idRif.value=='') 
            idError.innerHTML='Debe insertar Número de Rif';        
        else if (idClave.value=='') 
            idError.innerHTML='Debe insertar Contraseña';        
        else 
            respond=true;            
    else if (idCombo.value==2)       
        if (idCedula.value=='') 
            idError.innerHTML='Debe insertar Cédula';        
        else if (idClave.value=='') 
            idError.innerHTML='Debe insertar Contraseña';        
        else 
            respond=true;           
   else if (idCombo.value==3)
        if (idEmail.value=='')
            idError.innerHTML='Debe insertar Correo';        
        else if (idClave.value=='') 
            idError.innerHTML='Debe insertar Contraseña';        
        else 
            respond=true;     
    else if (idCombo.value==4)
       if (idUsuario.value=='')
            idError.innerHTML='Debe insertar Usuario';        
        else if (idClave.value=='')
            idError.innerHTML='Debe insertar Contraseña';        
        else 
            respond=true;   
             
   if (respond){
   	if (idCaptcha.value=='' && bCaptcha){
		idErrorCaptcha.innerHTML='Debe insertar Código';   
		respond = false;
		return respond;
   	}
	else{
		if (bCaptcha)
			respCaptcha=validCaptcha();
		if (respCaptcha){
       			document.forms[0].action='../admin_acceso/login/';
       			document.forms[0].submit();
		}
		else{
			idErrorCaptcha.innerHTML='Insertar Código Correcto';
                        document.getElementById('txtInput').value='';
			respond = false;
			return respond;
		}
	}
   }
   else
       return respond; 
}

function loginOptOnOff()
{ 
    if (document.getElementById('div_login').style.display ='none')
        document.getElementById('div_login').style.display ='block';   
    if (document.getElementById('id_opcion').value==0){  
        document.getElementById('div_rif').style.display = 'none';
        document.getElementById('div_ced').style.display = 'none';
        document.getElementById('div_mail').style.display = 'none';
        document.getElementById('div_user_name').style.display ='none';
        document.getElementById('div_login').style.display ='none'; 
    } 
    else if (document.getElementById('id_opcion').value==1){  
        document.getElementById('div_rif').style.display = 'block';
        document.getElementById('div_ced').style.display = 'none';
        document.getElementById('div_mail').style.display = 'none';
        document.getElementById('div_user_name').style.display ='none';
    }
    else if (document.getElementById('id_opcion').value==2){
        document.getElementById('div_rif').style.display = 'none';
        document.getElementById('div_ced').style.display = 'block';
        document.getElementById('div_mail').style.display = 'none';
        document.getElementById('div_user_name').style.display ='none';
    }
    else if (document.getElementById('id_opcion').value==3){
        document.getElementById('div_rif').style.display = 'none';
        document.getElementById('div_ced').style.display = 'none';
        document.getElementById('div_mail').style.display = 'block';
        document.getElementById('div_user_name').style.display ='none';
    }
    else if (document.getElementById('id_opcion').value==4){
        document.getElementById('div_rif').style.display = 'none';
        document.getElementById('div_ced').style.display = 'none';
        document.getElementById('div_mail').style.display = 'none';
        document.getElementById('div_user_name').style.display ='block';
    }
}

function accesoEnter(cod,e)
{
    if(cod!='' && pressTheEnterKey(e))                
        validarForm();    
}

