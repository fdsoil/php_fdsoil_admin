<?php
use \myApp0\Usuario as Usuario;

class SubIndex
{
    public function execute()
    {  
        $arrayLogin = Usuario::tipoLogin();
        $Post['usuario'] = $arrayLogin['usuario'];
        $Post['campo'] = $arrayLogin['campo']; 
        $Post['clave'] = md5($_POST['clave']);
        $result = Usuario::validarAcceso($Post);
        $path = "../../../";
        $location = "Location:" . $path . strtolower( $_SESSION['aMyApp'][0] );
        if ( $row = \FDSoil\DbFunc::fetchArray($result) ) {
            $diferencia = Usuario::usuarioClaveUltimoCambio($row);
            if ( $row['id_status'] == 1 ) {
                Usuario::inicioSession($row);
                if ( ($_SESSION['maxMonthWithKey'] != '') && ( intval($diferencia) > intval($_SESSION['maxMonthWithKey']) ) )            
                    header( $location . "/admin_usuario_change_pswd_forced/" );
                else {            
                    //include_once( $path . $_SESSION['aMyApp'][0] . '/config/inicio_sesion_aux.php' );
                    include_once( $path . 'config/inicio_sesion_aux.php' );
                    header( "Location:" . $path . $_SESSION['pag_ini_default'] );
                }
            }else
                header( $location . "/admin_acceso/2" );
        }else
           header( $location . "/admin_acceso/1" );
    } 
}

