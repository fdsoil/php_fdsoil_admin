<?php
use \FDSoil\Func;
use \FDSoil\DbFunc;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = isset($_POST['id'])?1:0;
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $xtpl->assign('ID', (isset($_POST['id']))?$_POST['id']:0);
        $result = \myApp0\Usuario::usuarioRow($_POST);
        $userRow = DbFunc::fetchAssoc($result);
        $xtpl->assign('ID_USUARIO',$userRow['id']);
        $xtpl->assign('USUARIO_USUARIO',$userRow['usuario']);
        $xtpl->assign('CORREO_USUARIO',$userRow['correo']);
        $xtpl->assign('NAC_CEDULA_USUARIO_BLOCK',($userRow['nacionalidad']!='')?"onfocus='this.blur()'":"");
        $xtpl->assign('NAC_CEDULA_USUARIO_READONLY',($userRow['nacionalidad']!='')?'readonly':'');
        $xtpl->assign('SELECTED_V',($userRow['nacionalidad']=='V')?'selected':'');
        $xtpl->assign('SELECTED_E',($userRow['nacionalidad']=='E')?'selected':'');
        $xtpl->assign('CEDULA_USUARIO_BLOCK',($userRow['ced']!='')?'readonly':'');
        $xtpl->assign('CEDULA_USUARIO', $userRow['ced']);
        $xtpl->assign('NOMBRE_USUARIO',$userRow['nombre']);
        $xtpl->assign('NOMBRE_USUARIO_READONLY',($userRow['nombre']!='')?'readonly':'');
        $xtpl->assign('APELLIDO_USUARIO',$userRow['apellido']);
        $xtpl->assign('APELLIDO_USUARIO_READONLY',($userRow['apellido']!='')?'readonly':'');
        $xtpl->assign('COD_CELULAR_USUARIO',substr($userRow['celular'], 0, 4));
        $xtpl->assign('COD_CELULAR_USUARIO_ONCLICK', 
        "pickOpen('celular_cod', 'id_celular_cod', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_cel/', 15, 25, 400, 100, 'si');");
        $xtpl->assign('CELULAR_USUARIO',substr($userRow['celular'], 4));
        $xtpl->assign('COD_TELEFONO1_USUARIO',substr($userRow['telefono1'], 0, 4));
        $xtpl->assign('COD_TELEFONO1_USUARIO_ONCLICK',
        "pickOpen('local_cod', 'id_local_cod', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_nac/', 15, 25, 400, 100, 'si');");
        $xtpl->assign('TELEFONO1_USUARIO',substr($userRow['telefono1'], 4));
        $xtpl->assign('COD_TELEFONO2_USUARIO',substr($userRow['telefono2'], 0, 4));
        $xtpl->assign('COD_TELEFONO2_USUARIO_ONCLICK',
        "pickOpen('local_cod2', 'id_local_cod2', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_nac/', 15, 25, 400, 100, 'si');");
        $xtpl->assign('TELEFONO2_USUARIO',substr($userRow['telefono2'], 4));
        $xtpl->assign('ID_ROL_USUARIO',$userRow['id_rol']);
        $xtpl->assign('ID_STATUS_USUARIO',$userRow['id_status']);
        $result = \myApp0\Usuario\Rol::rolList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_ROL_USUARIO', $row[0]);
            $xtpl->assign('DES_ROL_USUARIO', $row[1]);
            if (isset($_POST['id']))
                $xtpl->assign('VAL_ROL_USUARIO', ($userRow['id_rol'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.row_rol');
        }
        $result = \myApp0\Usuario\Rol\Menu::menuStatusList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            if (isset($_POST['id']))
                $xtpl->assign('VAL_STATUS', ($userRow['id_status'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.row_status');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return"], ["btnName"=>"Save", "btnClick"=>"val_envio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

