<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = "";
        $aView['load'] = "[]";
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $xtpl->assign('INTERFAZ_1', 'block');
        $xtpl->assign('INTERFAZ_2', 'none');
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

