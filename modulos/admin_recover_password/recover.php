<?php
use \FDSoil\Func as Func;
use \FDSoil\Usuario as Usuario;

class SubIndex
{
    public function execute()
    {
        $msj='V';
        $Post['cedula'] = $_POST['inicial'] . $_POST['cedula'];
        $Post['correo'] = $_POST['correo'];
        $Post['respuesta'] = $_POST['respuesta'];
        $nueva_clave=\FDSoil\Func::randomString(8,true,true,true,true); 
        $resultado = Usuario::consultarUsuario($Post);
        $row = \FDSoil\DbFunc::fetchRow($resultado);
        if ($row[6] == $Post['respuesta']) {
	    $nombre=$row[3] . " " . $row[4];
            include_once("../../../".$_SESSION['myApp']."/config/type_of_login.php");  
            switch ($typeOfLogin) {
                case 2:
                    $tipoDeLogin=$row[2];
                    break;
                case 3:
                    $tipoDeLogin=$row[1];
                    break;
                default:
                    $tipoDeLogin=$row[0];
                    break;
            }              
            $usuario=$tipoDeLogin;
            $email_destino=$Post['correo'];
            $tipo_mensaje='msj_recuperacion';
            $Post['clave'] = md5($nueva_clave);
            $Post['id']=$row[7];
	    //$Post=$obj->validateInput($Post);
            Usuario::recoverPassWord($Post);        
	    include_once('../../packs/email/send_email.php');
            correo_enviar($nombre,$usuario,$nueva_clave,$email_destino,$tipo_mensaje);
	    $msj='D';
        }
        Func::adminMsj($msj,2);  
    }
}

