<?php
use \FDSoil\Usuario as Usuario;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = "";
        $aView['load'] = "[]";
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);    
        $preguntaSecreta = Usuario::preguntaSecreta();
        $xtpl->assign('DISPLAY1', $preguntaSecreta == '' ? 'none' : '' );
        $xtpl->assign('DISPLAY2', $preguntaSecreta == '' ? '' : 'none' );
        if ( $preguntaSecreta == '' ){
            $result = Usuario::listarPreguntaSeguridad();    
            while ($row = \FDSoil\DbFunc::fetchAssoc($result)) {
                $xtpl->assign('PREGUNTA', $row['descripcion']);
                $xtpl->parse('main.row');
            }    
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

