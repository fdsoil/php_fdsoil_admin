<?php
use \FDSoil\Usuario as Usuario;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);  
        $xtpl->assign('ID', $_SESSION['id_usuario']);
        $aId['id']=$_SESSION['id_usuario'];
        $result = Usuario::usuarioRow($aId);
        $userRow = DbFunc::fetchAssoc($result);    
        $xtpl->assign('ID_USUARIO',$userRow['id']);
        $xtpl->assign('USUARIO_USUARIO',$userRow['usuario']);
        $xtpl->assign('CORREO_USUARIO',$userRow['correo']);
        $xtpl->assign('CEDULA_USUARIO',$userRow['cedula']);
        $xtpl->assign('NOMBRE_USUARIO',$userRow['nombre']);
        $xtpl->assign('APELLIDO_USUARIO',$userRow['apellido']);
        $xtpl->assign('COD_CELULAR_USUARIO',substr($userRow['celular'], 0, 4));
        $xtpl->assign('COD_CELULAR_USUARIO_ONCLICK',
        "pickOpen('celular_cod', 'id_celular_cod', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_cel/', 15, 25, 400, 100, 'si');");
        $xtpl->assign('CELULAR_USUARIO',substr($userRow['celular'], 4));
        $xtpl->assign('COD_TELEFONO1_USUARIO',substr($userRow['telefono1'], 0, 4));
        $xtpl->assign('COD_TELEFONO1_USUARIO_ONCLICK',
        "pickOpen('local_cod', 'id_local_cod', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_nac/', 15, 25, 450, 275, 'si');");
        $xtpl->assign('TELEFONO1_USUARIO',substr($userRow['telefono1'], 4));
        $xtpl->assign('COD_TELEFONO2_USUARIO',substr($userRow['telefono2'], 0, 4));
        $xtpl->assign('COD_TELEFONO2_USUARIO_ONCLICK',
        "pickOpen('local_cod2', 'id_local_cod2', '../../../".$_SESSION['appOrg']."/pickList/control/tel_cod_area_nac/', 15, 25, 450, 295, 'si');");
        $xtpl->assign('TELEFONO2_USUARIO',substr($userRow['telefono2'], 4));
        $xtpl->assign('ID_ROL_USUARIO',$userRow['id_rol']);
        $xtpl->assign('ID_STATUS_USUARIO',$userRow['id_status']);
        $xtpl->assign('PREGUNTA_SEGURIDAD_USUARIO',$userRow['pregunta_seguridad']);
        $xtpl->assign('RESPUESTA_SEGURIDAD_USUARIO',$userRow['respuesta_seguridad']);
        $result = Usuario::listarPreguntaSeguridad();    
        while ($row = DbFunc::fetchAssoc($result)) {
            $xtpl->assign('PREGUNTA', $row['descripcion']);
            if (isset($_SESSION['id_usuario']))
                $xtpl->assign('PREGUNTA_SELECTED', ($userRow['pregunta_seguridad'] == $row['descripcion']) ? 'selected' : '');
            $xtpl->parse('main.row');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Exit"], ["btnName"=>"Save", "btnClick"=>"val_envio();"]]);     	
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

